package com.ifeve.ifeveforandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ifeve.ifeveforandroid.service.ServiceHelper;
import com.ifeve.ifeveforandroid.service.SyncService;
import com.ifeve.ifeveforandroid.util.MyResultReceiver;

import java.util.Date;

/**
 * Created by penkzhou on 14-8-24.
 */
public class SplashActivity extends ActionBarActivity implements MyResultReceiver.Receiver {
    public static final String FIRST_TIME = "first_time";
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            //send user info that the network is not well.
            progressBar.setVisibility(View.GONE);
            networkTip.setVisibility(View.VISIBLE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(FIRST_TIME, true);
            editor.commit();
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                SplashActivity.this.finish();
            }
        }
    };
    private static final String TAG = SplashActivity.class.getSimpleName();
    private MyResultReceiver myResultReceiver;
    private SharedPreferences prefs;
    private ProgressBar progressBar;
    private TextView networkTip;
    private int finish = 0;
    private long start;
    private long end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate ");
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progressBar = (ProgressBar) findViewById(R.id.splash_progressbar);
        networkTip = (TextView) findViewById(R.id.networkTip);
        loadDataForTheFirstTime();
    }

    public void loadDataForTheFirstTime() {
        Log.i(TAG, "loadDataForTheFirstTime ");
        start = new Date().getTime();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Log.i(TAG, "load for the first time before" + prefs.getBoolean(FIRST_TIME, true));
        if (prefs.getBoolean(FIRST_TIME, true)) {
            Log.i(TAG, "app run for the first time, then load the data for the first time.");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(FIRST_TIME, false);
            editor.commit();
            Log.i(TAG, "load for the first time after " + prefs.getBoolean(FIRST_TIME, true));
            registerReceiver();
            final String tagUrl = "http://ifeve.com/api/get_tag_index/";
            final String authorUrl = "http://ifeve.com/api/get_author_index/";
            final String categoryUrl = "http://ifeve.com/api/get_category_index/";
            ServiceHelper.execute(getApplicationContext(), myResultReceiver, MainActivity.CATEGORY_TOKEN, categoryUrl);
            ServiceHelper.execute(getApplicationContext(), myResultReceiver, MainActivity.TAG_TOKEN, tagUrl);
            ServiceHelper.execute(getApplicationContext(), myResultReceiver, MainActivity.AUTHOR_TOKEN, authorUrl);
        } else {
            gotoMainActivity();
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        Log.i(TAG, "onReceiveResult ");
        switch (resultCode) {
            case SyncService.STATUS_ERROR:
                Log.i(TAG, "onReceiveResult STATUS_ERROR ");
                break;
            case SyncService.STATUS_FINISHED:
                finish++;
                Log.i(TAG, "onReceiveResult STATUS_FINISHED : " + finish);
                break;
            case SyncService.STATUS_RUNNING:
                Log.i(TAG, "onReceiveResult STATUS_RUNNING ");
                break;
        }
        if (finish == 3) {
            end = new Date().getTime();
            Log.i(TAG, "succeed , the time is " + (end - start));
            gotoMainActivity();
        }
    }

    @Override
    public void registerReceiver() {
        Log.i(TAG, "registerReceiver times");
        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);
        //send user a info about the failed data fetch.
        handler.sendEmptyMessageDelayed(0, 30 * 1000);
    }

    public void gotoMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }

    public static class SettingsActivity extends PreferenceActivity
            implements Preference.OnPreferenceChangeListener {

        // since we use the preference change initially to populate the summary
        // field, we'll ignore that change at start of the activity
        boolean mBindingPreference;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            // Add 'general' preferences, defined in the XML file
            // TODO: Add preferences from XML
            addPreferencesFromResource(R.xml.preference);
            // For all preferences, attach an OnPreferenceChangeListener so the UI summary can be
            // updated when the preference changes.
            // TODO: Add preferences
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_post_order_key)));
        }

        /**
         * Attaches a listener so the summary is always updated with the preference value.
         * Also fires the listener once, to initialize the summary (so it shows up before the value
         * is changed.)
         */
        private void bindPreferenceSummaryToValue(Preference preference) {
            mBindingPreference = true;
            // Set the listener to watch for value changes.
            preference.setOnPreferenceChangeListener(this);

            // Trigger the listener immediately with the preference's
            // current value.
            onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.getContext())
                            .getString(preference.getKey(), ""));
            mBindingPreference = false;
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();
            // are we starting the preference activity?
            if (!mBindingPreference) {
                if (preference.getKey().equals(getString(R.string.pref_post_order_key))) {
                    //do something when change the preference
                } else {
                    // notify code that the postlist may be impacted
                }
            }
            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list (since they have separate labels/values).
                ListPreference listPreference = (ListPreference) preference;
                int prefIndex = listPreference.findIndexOfValue(stringValue);
                if (prefIndex >= 0) {
                    preference.setSummary(listPreference.getEntries()[prefIndex]);
                }
            } else {
                // For other preferences, set the summary to the value's simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }

    }
}
