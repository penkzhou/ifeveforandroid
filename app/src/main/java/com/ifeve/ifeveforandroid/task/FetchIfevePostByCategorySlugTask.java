package com.ifeve.ifeveforandroid.task;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.ifeve.ifeveforandroid.model.IfevePost;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

/**
 * Created by penkzhou on 14-8-20.
 */
public class FetchIfevePostByCategorySlugTask extends AsyncTask<String, Vector<IfevePost>, Vector<IfevePost>> {
    private static final String TAG = FetchIfevePostByCategorySlugTask.class.getSimpleName();

    @Override
    protected Vector<IfevePost> doInBackground(String... params) {
        if (params.length == 0){
            return null;
        }
        // These two need to be declared outside the try/catch
// so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

// Will contain the raw JSON response as a string.
        String postJsonStr = null;


        int page = 1;
        int postCount = 15;
        try {
            final String POST_BASE_RUL = "http://ifeve.com/api/get_category_posts/?";
            final String ID_PARAM = "id";
            final String PAGE_PARAM = "page";
            final String COUNT_PARAM = "count";
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at POST's post API page, at
            // http://openpostmap.org/API#post
            Uri buildUri = Uri.parse(POST_BASE_RUL).buildUpon()
                    .appendQueryParameter(ID_PARAM,params[0])
                    .appendQueryParameter(PAGE_PARAM,Integer.toString(page))
                    .appendQueryParameter(COUNT_PARAM,Integer.toString(postCount))
                    .build();

            URL url = new URL(buildUri.toString());
            // Create the request to OpenWeatherMap, and open the connection
            Log.i(TAG, url.toString());
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                postJsonStr = null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                postJsonStr = null;
            }
            postJsonStr = buffer.toString();
            Log.i(TAG,postJsonStr);
            return getPostDataFromJson(postJsonStr, postCount, params[0]);
        } catch (IOException e) {
            Log.e("PlaceholderFragment", "Error ", e);
            // If the code didn't successfully get the post data, there's no point in attemping
            // to parse it.
            postJsonStr = null;
        } catch (JSONException e) {
            e.printStackTrace();
        } finally{
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }
        return null;
    }

    private Vector<IfevePost> getPostDataFromJson(String postJsonStr, int postCount, String param) throws JSONException{
        // Location information


        final String POST_LIST = "posts";


        final String POST_PAGES = "pages";

        final String POST_CURRENT_PAGE_COUNT = "count";


        final String POST_AUTHOR = "author";
        final String POST_AUTHOR_NAME = "name";
        final String POST_AUTHOR_ID = "id";

        final String POST_CATEGORY = "categories";
        final String POST_CATEGORY_ID = "id";
        final String POST_CATEGORY_SLUG = "slug";
        final String POST_CATEGORY_TITLE = "title";


        final String POST_COMMENT_COUNT = "comment_count";
        final String POST_COMMENT_STATUS = "status";


        final String POST_CONTENT = "content";
        final String POST_DATE = "date";
        final String POST_MODIFY = "modified";
        final String POST_SLUG = "slug";
        final String POST_STATUS = "status";
        final String POST_TYPE = "type";
        final String POST_TITLE = "title";




        JSONObject postJson = new JSONObject(postJsonStr);
        JSONArray postArray = postJson.getJSONArray(POST_LIST);

        int pages = postJson.getInt(POST_PAGES);
        int currentCount = postJson.getInt(POST_CURRENT_PAGE_COUNT);

        Vector<IfevePost> cvVector = new Vector<IfevePost>(postArray.length());

        String[] resultStrs = new String[postCount];
        for(int i = 0; i < postArray.length(); i++) {
            // These are the values that will be collected.


            String author_name;
            int author_id;

            int category_id;
            String category_title;

            int comment_count;
            String comment_status;


            String post_content;
            String post_date;
            String post_modified_date;
            String post_slug;
            String post_status;
            String post_type;
            String post_title;

//            JSONArray categoryArray = postJson.getJSONArray(POST_CATEGORY);
//            for(int j = 0;j<categoryArray.length();j++){
//                JSONObject categoryJson = categoryArray.getJSONObject(j);
//
//            }
            // Get the JSON object representing the day
            JSONObject postItem = postArray.getJSONObject(i);

            author_name = postItem.getJSONObject(POST_AUTHOR).getString(POST_AUTHOR_NAME);
            author_id = postItem.getJSONObject(POST_AUTHOR).getInt(POST_AUTHOR_ID);

            comment_count = postItem.getInt(POST_COMMENT_COUNT);

            // The date/time is returned as a long.  We need to convert that
            // into something human-readable, since most people won't read "1400356800" as
            // "this saturday".
            post_content = postItem.getString(POST_CONTENT);
            post_date = postItem.getString(POST_DATE);
            post_modified_date = postItem.getString(POST_MODIFY);
            post_slug = postItem.getString(POST_SLUG);
            post_status = postItem.getString(POST_STATUS);
            post_type = postItem.getString(POST_TYPE);
            post_title = postItem.getString(POST_TYPE);

            IfevePost post = new IfevePost();
            post.setAuthor_id(author_id);
            post.setAuthor_name(author_name);
            post.setComment_count(comment_count);
            post.setPost_content(post_content);
            post.setPost_date(post_date);
            post.setPost_modified_date(post_modified_date);
            post.setPost_slug(post_slug);
            post.setPost_status(post_status);
            post.setPost_type(post_type);
            post.setPost_title(post_title);

//            ContentValues postValues = new ContentValues();
//
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_LOC_KEY, locationID);
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_DATETEXT,
//                    WeatherContract.getDbDateString(new Date(dateTime * 1000L)));
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_HUMIDITY, humidity);
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_PRESSURE, pressure);
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED, windSpeed);
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_DEGREES, windDirection);
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP, high);
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP, low);
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_SHORT_DESC, description);
//            postValues.put(WeatherContract.WeatherEntry.COLUMN_WEATHER_ID, postId);

            cvVector.add(post);

        }
//        if (cvVector.size() > 0) {
//            ContentValues[] cvArray = new ContentValues[cvVector.size()];
//            cvVector.toArray(cvArray);
//            int rowsInserted = mContext.getContentResolver()
//                    .bulkInsert(WeatherContract.WeatherEntry.CONTENT_URI, cvArray);
//            Log.v(TAG, "inserted " + rowsInserted + " rows of post data");
//        }
        return cvVector;
    }


}
