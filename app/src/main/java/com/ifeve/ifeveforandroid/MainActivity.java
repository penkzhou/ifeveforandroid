package com.ifeve.ifeveforandroid;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ifeve.ifeveforandroid.adapter.NavigationAdapter;
import com.ifeve.ifeveforandroid.adapter.NavigationItemAdapter;
import com.ifeve.ifeveforandroid.fragment.AuthorsTagFragment;
import com.ifeve.ifeveforandroid.fragment.IndexFragment;
import com.ifeve.ifeveforandroid.fragment.RecentPostListFragment;
import com.ifeve.ifeveforandroid.service.ServiceHelper;
import com.ifeve.ifeveforandroid.util.MenuUtil;
import com.ifeve.ifeveforandroid.util.MyResultReceiver;


public class MainActivity extends ActionBarActivity implements MyResultReceiver.Receiver {

    public static final String LAST_POSITION = "last_position";
    public static final String FIRST_TIME = "first_time";
    public static final int AUTHOR_TOKEN = 1;
    public static final int TAG_TOKEN = 2;
    public static final int CATEGORY_TOKEN = 3;
    public static final int POST_TOKEN = 4;
    public static final int POST_CLEAN_TOKEN = 5;
    public static final int REFRESH_POST_TOKEN = 6;
    public static final int REFRESH_POST_CLEAN_TOKEN = 7;
    private static final String TAG = MainActivity.class.getSimpleName();
    public static int[] iconNavigation = new int[]{
            0, 0, 0, 0, 0, R.drawable.ic_action_settings, R.drawable.ic_action_about};
    private int counterItemDownloads;
    private int lastPosition = 0;
    private DrawerLayout layoutDrawer;
    private LinearLayout linearDrawer;
    private View.OnClickListener userOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            layoutDrawer.closeDrawer(linearDrawer);
        }
    };
    private ListView listDrawer;
    private RelativeLayout userDrawer;
    private String[] menuItems;
    private MyResultReceiver myResultReceiver;
    private NavigationAdapter navigationAdapter;
    private ActionBarDrawerToggleCompat drawerToggle;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        counterItemDownloads = 9;

        getSupportActionBar().setHomeButtonEnabled(true);

        mTitle = getTitle();
        listDrawer = (ListView) findViewById(R.id.listDrawer);
        linearDrawer = (LinearLayout) findViewById(R.id.linearDrawer);
        layoutDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (listDrawer != null) {
            navigationAdapter = new NavigationAdapter(getApplicationContext());
            menuItems = getResources().getStringArray(R.array.nav_menu_items);

            for (int i = 0; i < menuItems.length; i++) {
                String title = menuItems[i];
                NavigationItemAdapter itemNavigation;
                itemNavigation = new NavigationItemAdapter(title, iconNavigation[i]);
                navigationAdapter.addItem(itemNavigation);
            }
        }

        userDrawer = (RelativeLayout) findViewById(R.id.userDrawer);
        userDrawer.setOnClickListener(userOnClick);

        listDrawer.setAdapter(navigationAdapter);
        listDrawer.setOnItemClickListener(new DrawerItemClickListener());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle = new ActionBarDrawerToggleCompat(this, layoutDrawer);
        layoutDrawer.setDrawerListener(drawerToggle);


        if (savedInstanceState != null) {
            setLastPosition(savedInstanceState.getInt(LAST_POSITION));

            if (lastPosition < 5) {
                navigationAdapter.resetarCheck();
                navigationAdapter.setChecked(lastPosition, true);
            }

        } else {
            setLastPosition(lastPosition);
            setFragmentList(lastPosition);
        }
        loadDataForTheFirstTime();
    }


    public void loadDataForTheFirstTime() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getBoolean(FIRST_TIME, true)) {
            Log.i(TAG, "app run for the first time, then load the data for the first time.");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(FIRST_TIME, false);
            editor.commit();

            final String tagUrl = "http://ifeve.com/api/get_tag_index/";
            final String authorUrl = "http://ifeve.com/api/get_author_index/";
            final String categoryUrl = "http://ifeve.com/api/get_category_index/";
            ServiceHelper.execute(getApplicationContext(), myResultReceiver, CATEGORY_TOKEN, categoryUrl);
            ServiceHelper.execute(getApplicationContext(), myResultReceiver, TAG_TOKEN, tagUrl);
            ServiceHelper.execute(getApplicationContext(), myResultReceiver, AUTHOR_TOKEN, authorUrl);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

    }

    @Override
    public void registerReceiver() {
        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);
    }

    public void setLastPosition(int position) {
        this.lastPosition = position;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(LAST_POSITION, lastPosition);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    private void setFragmentList(int position) {
        //setTitleFragments(position);
        mTitle = menuItems[position];
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new RecentPostListFragment();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                break;
            case 1:
                fragment = new IndexFragment();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                break;
            case 2:
                fragment = new AuthorsTagFragment();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                break;
            case 3:
                Intent settingIntent = new Intent(this, SplashActivity.SettingsActivity.class);
                startActivity(settingIntent);
                break;
            case 4:
                Intent aboutIntent = new Intent(this, AboutActivity.class);
                startActivity(aboutIntent);
                break;

            default:
                Toast.makeText(getApplicationContext(), "implement other fragments here", Toast.LENGTH_SHORT).show();
                break;
        }


        if (position < 5) {
            navigationAdapter.resetarCheck();
            navigationAdapter.setChecked(position, true);
        }
    }

    public void setTitleFragments(int position) {
        getSupportActionBar().setTitle(menuItems[position]);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!layoutDrawer.isDrawerOpen(linearDrawer)) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_about:
                Intent aboutIntent = new Intent(this, AboutActivity.class);
                startActivity(aboutIntent);
                return true;
            case R.id.action_settings:
                Intent settingIntent = new Intent(this, SplashActivity.SettingsActivity.class);
                startActivity(settingIntent);
                return true;
            case MenuUtil.HOME:
                if (layoutDrawer.isDrawerOpen(linearDrawer)) {
                    layoutDrawer.closeDrawer(linearDrawer);
                } else {
                    layoutDrawer.openDrawer(linearDrawer);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public int getCounterItemDownloads() {
        return counterItemDownloads;
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            setLastPosition(position);
            setFragmentList(lastPosition);
            layoutDrawer.closeDrawer(linearDrawer);
        }
    }

    private class ActionBarDrawerToggleCompat extends ActionBarDrawerToggle {

        public ActionBarDrawerToggleCompat(Activity mActivity, DrawerLayout mDrawerLayout) {
            super(
                    mActivity,
                    mDrawerLayout,
                    R.drawable.ic_navigation_drawer,
                    R.string.navigation_drawer_open,
                    R.string.navigation_drawer_close);
        }

        @Override
        public void onDrawerClosed(View view) {
            supportInvalidateOptionsMenu();
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            getSupportActionBar().setTitle(getString(R.string.app_name));
            navigationAdapter.notifyDataSetChanged();
            supportInvalidateOptionsMenu();
        }
    }


}
