package com.ifeve.ifeveforandroid.util;

/**
 * Created by penkzhou on 14-8-23.
 * <p/>
 * 获取最近的文章：http://ifeve.com/api/get_recent_posts/
 * 获取一篇文章： http://ifeve.com/api/get_post/?post_id=13840
 * 获取某个标签的文章：http://ifeve.com/api/get_tag_posts/?tag_slug=java
 * 查询所有分类：http://ifeve.com/api/get_category_index/
 * 查询某个分类的文章：http://ifeve.com/api/get_category_posts/?slug=web（输入分类名称）
 * 查询所有标签：http://ifeve.com/api/get_tag_index/
 * 按照某个关键字搜索文章：http://ifeve.com/api/get_search_results/?search=java
 * 查询所有作者：http://ifeve.com/api/get_author_index/
 * 查询某个作者的文章：http://ifeve.com/api/get_author_posts/?slug=sunqi（输入作者登陆ID）
 */
public interface IfeveAPI {
    String BASE_URL = "http://ifeve.com/api";
    String GET_RECENT_POSTS = BASE_URL + "/get_recent_posts/";
    String GET_POST_BY_POST_ID = BASE_URL + "/get_post/?post_id=";
    String GET_POSTS_BY_TAG_ID = BASE_URL + "/get_tag_posts/?tag_id=";
    String GET_POSTS_BY_CATEGORY_ID = BASE_URL + "/get_category_posts/?id=";
    String GET_POSTS_BY_CATEGORY = BASE_URL + "/get_category_posts/";
    String GET_POSTS_BY_AUTHOR_ID = BASE_URL + "/get_author_posts/?id=";
    String GET_ALL_TAGS = BASE_URL + "/get_tag_index/";
    String GET_ALL_AUTHORS = BASE_URL + "/get_author_index/";
    String GET_ALL_CATEGORIES = BASE_URL + "/get_category_index/";
    String ID_PARAM = "id";
    String PAGE_PARAM = "page";
}
