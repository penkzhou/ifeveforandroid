package com.ifeve.ifeveforandroid.provider;

import android.app.SearchManager;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.ifeve.ifeveforandroid.provider.IfeveContract.Authors;
import com.ifeve.ifeveforandroid.provider.IfeveContract.AuthorsColumns;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Categories;
import com.ifeve.ifeveforandroid.provider.IfeveContract.CategoriesColumns;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Comments;
import com.ifeve.ifeveforandroid.provider.IfeveContract.CommentsColumns;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Posts;
import com.ifeve.ifeveforandroid.provider.IfeveContract.PostsColumns;
import com.ifeve.ifeveforandroid.provider.IfeveContract.SyncColumns;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Tags;
import com.ifeve.ifeveforandroid.provider.IfeveContract.TagsColumns;

/**
 * Created by penkzhou on 14-8-20.
 */
public class IfeveDatabase extends SQLiteOpenHelper {
    private final static String TAG = "IfeveDatabase";
    private final static String DATABASE_NAME = "post.db";
    private final static int DATABASE_VERSION = 1;


    public IfeveDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Create triggers that automatically build {@link Tables#POSTS_SEARCH}
     * as values are changed in {@link Tables#POSTS}.
     */
    private static void createPostsSearch(SQLiteDatabase db) {
        // Using the "porter" tokenizer for simple stemming, so that
        // "frustration" matches "frustrated."

        db.execSQL("CREATE VIRTUAL TABLE " + Tables.POSTS_SEARCH + " USING fts3("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PostsSearchColumns.BODY + " TEXT NOT NULL,"
                + PostsSearchColumns.POST_ID
                + " TEXT NOT NULL " + References.POST_ID + ","
                + "UNIQUE (" + PostsSearchColumns.POST_ID + ") ON CONFLICT REPLACE,"
                + "tokenize=porter)");

        // TODO: handle null fields in body, which cause trigger to fail
        // TODO: implement update trigger, not currently exercised

        db.execSQL("CREATE TRIGGER " + Triggers.POSTS_SEARCH_INSERT + " AFTER INSERT ON "
                + Tables.POSTS + " BEGIN INSERT INTO " + Qualified.POSTS_SEARCH + " "
                + " VALUES(new." + IfeveContract.Posts.POST_ID + ", " + Subquery.POSTS_BODY + ");"
                + " END;");

        db.execSQL("CREATE TRIGGER " + Triggers.POSTS_SEARCH_DELETE + " AFTER DELETE ON "
                + Tables.POSTS + " BEGIN DELETE FROM " + Tables.POSTS_SEARCH + " "
                + " WHERE " + Qualified.POSTS_SEARCH_POST_ID + "=old." + IfeveContract.Posts.POST_ID
                + ";" + " END;");

        db.execSQL("CREATE TRIGGER " + Triggers.POSTS_SEARCH_UPDATE
                + " AFTER UPDATE ON " + Tables.POSTS
                + " BEGIN UPDATE sessions_search SET " + PostsSearchColumns.BODY + " = "
                + Subquery.POSTS_BODY + " WHERE session_id = old.session_id"
                + "; END;");

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Tables.CATEGORIES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SyncColumns.UPDATED + " INTEGER NOT NULL,"
                + CategoriesColumns.CATEGORY_ID + " TEXT NOT NULL,"
                + CategoriesColumns.CATEGORY_DESC + " TEXT,"
                + CategoriesColumns.CATEGORY_PARENT + " TEXT,"
                + CategoriesColumns.CATEGORY_POST_COUNT + " INTEGER,"
                + CategoriesColumns.CATEGORY_SLUG + " TEXT,"
                + CategoriesColumns.CATEGORY_TITLE + " TEXT,"
                + "UNIQUE (" + CategoriesColumns.CATEGORY_ID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.AUTHORS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SyncColumns.UPDATED + " INTEGER NOT NULL,"
                + AuthorsColumns.AUTHOR_ID + " TEXT NOT NULL,"
                + AuthorsColumns.AUTHOR_DESC + " TEXT,"
                + AuthorsColumns.AUTHOR_FIRST_NAME + " TEXT,"
                + AuthorsColumns.AUTHOR_LAST_NAME + " TEXT,"
                + AuthorsColumns.AUTHOR_NAME + " TEXT,"
                + AuthorsColumns.AUTHOR_NICKNAME + " TEXT,"
                + AuthorsColumns.AUTHOR_SLUG + " TEXT,"
                + AuthorsColumns.AUTHOR_URL + " TEXT,"
                + "UNIQUE (" + AuthorsColumns.AUTHOR_ID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.POSTS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SyncColumns.UPDATED + " INTEGER NOT NULL,"
                + PostsColumns.POST_ID + " TEXT NOT NULL,"
                + Posts.AUTHOR_ID + " TEXT " + References.AUTHOR_ID + ","
                + PostsColumns.POST_TITLE + " TEXT,"
                + PostsColumns.POST_SLUG + " TEXT,"
                + PostsColumns.POST_URL + " TEXT,"
                + PostsColumns.POST_EXCERPT + " TEXT,"
                + PostsColumns.POST_CONTENT + " TEXT,"
                + PostsColumns.POST_COMMENT_COUNT + " INTEGER,"
                + PostsColumns.POST_COMMENT_STATUS + " TEXT,"
                + PostsColumns.POST_CREATE_DATE + " DATETIME,"
                + PostsColumns.POST_MODIFY_DATE + " DATETIME,"
                + PostsColumns.POST_STATUS + " TEXT,"
                + PostsColumns.POST_TYPE + " TEXT,"
                + PostsColumns.POST_TITLE_PLAIN + " TEXT,"
                + PostsColumns.POST_VIEW_COUNT + " INTEGER,"
                + "UNIQUE (" + PostsColumns.POST_ID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.TAGS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SyncColumns.UPDATED + " INTEGER NOT NULL,"
                + TagsColumns.TAG_ID + " TEXT NOT NULL,"
                + TagsColumns.TAG_DESC + " TEXT,"
                + TagsColumns.TAG_POST_COUNT + " INTEGER,"
                + TagsColumns.TAG_SLUG + " TEXT,"
                + TagsColumns.TAG_TITLE + " TEXT,"
                + "UNIQUE (" + TagsColumns.TAG_ID + ") ON CONFLICT REPLACE)");


        db.execSQL("CREATE TABLE " + Tables.COMMENTS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SyncColumns.UPDATED + " INTEGER NOT NULL,"
                + CommentsColumns.COMMENT_ID + " TEXT NOT NULL,"
                + CommentsColumns.COMMENT_CONTENT + " TEXT,"
                + CommentsColumns.COMMENT_DATE + " DATETIME,"
                + CommentsColumns.COMMENT_NAME + " TEXT,"
                + CommentsColumns.COMMENT_PARENT + " TEXT,"
                + CommentsColumns.COMMENT_URL + " TEXT,"
                + "UNIQUE (" + CommentsColumns.COMMENT_ID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.POSTS_TAGS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PostsTags.POST_ID + " TEXT NOT NULL " + References.POST_ID + ","
                + PostsTags.TAG_ID + " TEXT NOT NULL " + References.TAG_ID + ","
                + "UNIQUE (" + PostsTags.POST_ID + ","
                + PostsTags.TAG_ID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.POSTS_CATEGORIES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PostsCategories.POST_ID + " TEXT NOT NULL " + References.POST_ID + ","
                + PostsCategories.CATEGORY_ID + " TEXT NOT NULL " + References.CATEGORY_ID + ","
                + "UNIQUE (" + PostsCategories.POST_ID + ","
                + PostsCategories.CATEGORY_ID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.POSTS_COMMENTS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PostsComments.POST_ID + " TEXT NOT NULL " + References.POST_ID + ","
                + PostsComments.COMMENT_ID + " TEXT NOT NULL " + References.COMMENT_ID + ","
                + "UNIQUE (" + PostsComments.POST_ID + ","
                + PostsComments.COMMENT_ID + ") ON CONFLICT REPLACE)");

        createPostsSearch(db);

        db.execSQL("CREATE TABLE " + Tables.SEARCH_SUGGEST + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SearchManager.SUGGEST_COLUMN_TEXT_1 + " TEXT NOT NULL)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.d(TAG, "onUpgrade() from " + oldVersion + " to " + newVersion);

        // NOTE: This switch statement is designed to handle cascading database
        // updates, starting at the current version and falling through to all
        // future upgrade cases. Only use "break;" when you want to drop and
        // recreate the entire database.
        int version = oldVersion;

//        switch (version) {
//            case VER_LAUNCH:
//                // Version 22 added column for session feedback URL.
//                db.execSQL("ALTER TABLE " + Tables.SESSIONS + " ADD COLUMN "
//                        + SessionsColumns.SESSION_FEEDBACK_URL + " TEXT");
//                version = VER_SESSION_FEEDBACK_URL;
//
//            case VER_SESSION_FEEDBACK_URL:
//                // Version 23 added columns for session official notes URL and slug.
//                db.execSQL("ALTER TABLE " + Tables.SESSIONS + " ADD COLUMN "
//                        + SessionsColumns.SESSION_NOTES_URL + " TEXT");
//                db.execSQL("ALTER TABLE " + Tables.SESSIONS + " ADD COLUMN "
//                        + SessionsColumns.SESSION_SLUG + " TEXT");
//                version = VER_SESSION_NOTES_URL_SLUG;
//        }

        Log.d(TAG, "after upgrade logic, at version " + version);
        if (version != DATABASE_VERSION) {
            Log.w(TAG, "Destroying old data during upgrade");

            db.execSQL("DROP TABLE IF EXISTS " + Tables.CATEGORIES);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.AUTHORS);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.POSTS);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.TAGS);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.COMMENTS);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.POSTS_CATEGORIES);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.POSTS_TAGS);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.POSTS_COMMENTS);

            db.execSQL("DROP TRIGGER IF EXISTS " + Triggers.POSTS_SEARCH_INSERT);
            db.execSQL("DROP TRIGGER IF EXISTS " + Triggers.POSTS_SEARCH_DELETE);
            db.execSQL("DROP TABLE IF EXISTS " + Tables.POSTS_SEARCH);


            db.execSQL("DROP TABLE IF EXISTS " + Tables.SEARCH_SUGGEST);

            onCreate(db);
        }
    }

    interface Tables {

        String POSTS = "posts";
        String AUTHORS = "authors";
        String CATEGORIES = "categories";
        String TAGS = "tags";
        String COMMENTS = "comments";

        String POSTS_CATEGORIES = "posts_categories";
        String POSTS_TAGS = "posts_tags";
        String POSTS_COMMENTS = "posts_comments";


        String POSTS_SEARCH = "posts_search";


        String SEARCH_SUGGEST = "search_suggest";


        String POSTS_JOIN_AUTHORS = "posts "
                + "LEFT OUTER JOIN authors ON posts.author_id=authors.author_id";

        String POSTS_TAGS_JOIN_TAGS = "posts_tags "
                + "LEFT OUTER JOIN tags ON posts_tags.tag_id=tags.tag_id";

        String POSTS_COMMENTS_JOIN_COMMENTS = "posts_comments "
                + "LEFT OUTER JOIN comments ON posts_comments.comment_id=comments.comment_id";


        String POSTS_TAGS_JOIN_POSTS_AUTHORS = "posts_tags "
                + "LEFT OUTER JOIN posts ON posts_tags.post_id=posts.post_id "
                + "LEFT OUTER JOIN authors ON posts.author_id=authors.author_id";

        String POSTS_COMMENTS_JOIN_POSTS_AUTHORS = "posts_comments "
                + "LEFT OUTER JOIN posts ON posts_comments.post_id=posts.post_id "
                + "LEFT OUTER JOIN authors ON posts.author_id=authors.author_id";

        String POSTS_CATEGORIES_JOIN_CATEGORIES = "posts_categories "
                + "LEFT OUTER JOIN categories ON posts_categories.category_id=categories.category_id";

        String POSTS_CATEGORIES_JOIN_POSTS_AUTHORS = "posts_categories "
                + "LEFT OUTER JOIN posts ON posts_categories.post_id=posts.post_id "
                + "LEFT OUTER JOIN authors ON posts.author_id=authors.author_id";

        String POSTS_SEARCH_JOIN_POSTS_AUTHORS = "posts_search "
                + "LEFT OUTER JOIN posts ON posts_search.post_id=posts.post_id "
                + "LEFT OUTER JOIN authors ON posts.author_id=authors.author_id";


    }

    private interface Triggers {
        String POSTS_SEARCH_INSERT = "posts_search_insert";
        String POSTS_SEARCH_DELETE = "posts_search_delete";
        String POSTS_SEARCH_UPDATE = "posts_search_update";
    }

    public interface PostsTags {
        String POST_ID = "post_id";
        String TAG_ID = "tag_id";
    }

    public interface PostsComments {
        String POST_ID = "post_id";
        String COMMENT_ID = "comment_id";
    }


    public interface PostsCategories {
        String POST_ID = "post_id";
        String CATEGORY_ID = "category_id";
    }


    interface PostsSearchColumns {
        String POST_ID = "post_id";
        String BODY = "body";
    }

    /**
     * Fully-qualified field names.
     */
    private interface Qualified {
        String POSTS_SEARCH_POST_ID = Tables.POSTS_SEARCH + "."
                + PostsSearchColumns.POST_ID;


        String POSTS_SEARCH = Tables.POSTS_SEARCH + "(" + PostsSearchColumns.POST_ID
                + "," + PostsSearchColumns.BODY + ")";
    }

    /**
     * {@code REFERENCES} clauses.
     */
    private interface References {
        String TAG_ID = "REFERENCES " + Tables.TAGS + "(" + Tags.TAG_ID + ")";
        String COMMENT_ID = "REFERENCES " + Tables.COMMENTS + "(" + Comments.COMMENT_ID + ")";
        String AUTHOR_ID = "REFERENCES " + Tables.AUTHORS + "(" + Authors.AUTHOR_ID + ")";
        String POST_ID = "REFERENCES " + Tables.POSTS + "(" + Posts.POST_ID + ")";
        String CATEGORY_ID = "REFERENCES " + Tables.CATEGORIES + "(" + Categories.CATEGORY_ID + ")";
    }

    private interface Subquery {
        /**
         * Subquery used to build the {@link PostsSearchColumns#BODY} string
         * used for indexing {@link Posts} content.
         */
        String POSTS_BODY = "(new." + Posts.POST_TITLE
                + "||'; '||new." + Posts.POST_CONTENT
                + "||'; '||" + "coalesce(new." + Posts.POST_EXCERPT + ", '')"
                + ")";


    }
}
