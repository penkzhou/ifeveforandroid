package com.ifeve.ifeveforandroid.provider;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.BaseColumns;
import android.util.Log;

import com.ifeve.ifeveforandroid.provider.IfeveContract.Authors;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Categories;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Comments;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Posts;
import com.ifeve.ifeveforandroid.provider.IfeveContract.SearchSuggest;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Tags;
import com.ifeve.ifeveforandroid.provider.IfeveDatabase.PostsCategories;
import com.ifeve.ifeveforandroid.provider.IfeveDatabase.PostsComments;
import com.ifeve.ifeveforandroid.provider.IfeveDatabase.PostsSearchColumns;
import com.ifeve.ifeveforandroid.provider.IfeveDatabase.PostsTags;
import com.ifeve.ifeveforandroid.provider.IfeveDatabase.Tables;
import com.ifeve.ifeveforandroid.util.SelectionBuilder;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by penkzhou on 14-8-22.
 */
public class IfeveProvider extends ContentProvider {

    public static final String QUERY_PARAMETER_LIMIT = "limit";

    private static final String TAG = "IfeveProvider";
    private static final boolean LOGV = Log.isLoggable(TAG, Log.VERBOSE);
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private static final int CATEGORIES = 200;
    private static final int CATEGORIES_ID = 201;
    private static final int CATEGORIES_ID_POSTS = 202;
    private static final int AUTHORS = 300;
    private static final int AUTHORS_ID = 301;
    private static final int AUTHORS_ID_POSTS = 302;
    private static final int POSTS = 400;
    private static final int POSTS_SEARCH = 402;
    private static final int POSTS_ID = 404;
    private static final int POSTS_ID_TAGS = 405;
    private static final int POSTS_ID_CATEGORIES = 406;
    private static final int POSTS_ID_COMMENTS = 407;
    private static final int TAGS = 500;
    private static final int TAGS_ID = 501;
    private static final int TAGS_ID_POSTS = 502;
    private static final int COMMENTS = 600;
    private static final int COMMENTS_ID = 601;
    private static final int COMMENTS_ID_POSTS = 602;
    private static final int SEARCH_SUGGEST = 800;
    private static final String MIME_XML = "text/xml";
    private IfeveDatabase mOpenHelper;

    /**
     * Build and return a {@link UriMatcher} that catches all {@link Uri}
     * variations supported by this {@link ContentProvider}.
     */
    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = IfeveContract.CONTENT_AUTHORITY;


        matcher.addURI(authority, "categories", CATEGORIES);
        matcher.addURI(authority, "categories/*", CATEGORIES_ID);
        matcher.addURI(authority, "categories/*/posts", CATEGORIES_ID_POSTS);

        matcher.addURI(authority, "authors", AUTHORS);
        matcher.addURI(authority, "authors/*", AUTHORS_ID);
        matcher.addURI(authority, "authors/*/posts", AUTHORS_ID_POSTS);

        matcher.addURI(authority, "posts", POSTS);
        matcher.addURI(authority, "posts/search/*", POSTS_SEARCH);
        matcher.addURI(authority, "posts/*", POSTS_ID);
        matcher.addURI(authority, "posts/*/tags", POSTS_ID_TAGS);
        matcher.addURI(authority, "posts/*/comments", POSTS_ID_COMMENTS);
        matcher.addURI(authority, "posts/*/categories", POSTS_ID_CATEGORIES);

        matcher.addURI(authority, "tags", TAGS);
        matcher.addURI(authority, "tags/*", TAGS_ID);
        matcher.addURI(authority, "tags/*/posts", TAGS_ID_POSTS);


        matcher.addURI(authority, "comments", COMMENTS);
        matcher.addURI(authority, "comments/*", COMMENTS_ID);
        matcher.addURI(authority, "comments/*/posts", COMMENTS_ID_POSTS);


        matcher.addURI(authority, "search_suggest_query", SEARCH_SUGGEST);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        final Context context = getContext();
        mOpenHelper = new IfeveDatabase(context);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Log.v(TAG, "query(uri=" + uri + ", proj=" + Arrays.toString(projection) + ")");
        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();

        final int match = sUriMatcher.match(uri);
        switch (match) {
            default: {
                // Most cases are handled with simple SelectionBuilder
                final SelectionBuilder builder = buildExpandedSelection(uri, match);
                final String normalLimit = uri.getQueryParameter(QUERY_PARAMETER_LIMIT);
                builder.where(selection, selectionArgs);
                return builder.query(db, projection, null, null, sortOrder, normalLimit);
            }
            case SEARCH_SUGGEST: {
                final SelectionBuilder builder = new SelectionBuilder();

                // Adjust incoming query to become SQL text match
                selectionArgs[0] = selectionArgs[0] + "%";
                builder.table(Tables.SEARCH_SUGGEST);
                builder.where(selection, selectionArgs);
                builder.map(SearchManager.SUGGEST_COLUMN_QUERY,
                        SearchManager.SUGGEST_COLUMN_TEXT_1);

                projection = new String[]{BaseColumns._ID, SearchManager.SUGGEST_COLUMN_TEXT_1,
                        SearchManager.SUGGEST_COLUMN_QUERY};

                final String limit = uri.getQueryParameter(SearchManager.SUGGEST_PARAMETER_LIMIT);
                return builder.query(db, projection, null, null, SearchSuggest.DEFAULT_SORT, limit);
            }
        }
    }

    @Override
    public String getType(Uri uri) {

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case CATEGORIES:
                return Categories.CONTENT_TYPE;
            case CATEGORIES_ID:
                return Categories.CONTENT_ITEM_TYPE;
            case CATEGORIES_ID_POSTS:
                return Posts.CONTENT_TYPE;
            case AUTHORS:
                return Authors.CONTENT_TYPE;
            case AUTHORS_ID:
                return Authors.CONTENT_ITEM_TYPE;
            case AUTHORS_ID_POSTS:
                return Posts.CONTENT_TYPE;
            case POSTS:
                return Posts.CONTENT_TYPE;
            case POSTS_SEARCH:
                return Posts.CONTENT_TYPE;
            case POSTS_ID:
                return Posts.CONTENT_ITEM_TYPE;
            case POSTS_ID_TAGS:
                return Tags.CONTENT_TYPE;
            case POSTS_ID_COMMENTS:
                return Comments.CONTENT_TYPE;
            case POSTS_ID_CATEGORIES:
                return Categories.CONTENT_TYPE;
            case TAGS:
                return Tags.CONTENT_TYPE;
            case TAGS_ID:
                return Tags.CONTENT_ITEM_TYPE;
            case TAGS_ID_POSTS:
                return Posts.CONTENT_TYPE;
            case COMMENTS:
                return Comments.CONTENT_TYPE;
            case COMMENTS_ID:
                return Comments.CONTENT_ITEM_TYPE;
            case COMMENTS_ID_POSTS:
                return Posts.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (LOGV) Log.v(TAG, "insert(uri=" + uri + ", values=" + values.toString() + ")");
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case CATEGORIES: {
                db.insertOrThrow(Tables.CATEGORIES, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Categories.buildCategoryUri(values.getAsString(Categories.CATEGORY_ID));
            }
            case AUTHORS: {
                db.insertOrThrow(Tables.AUTHORS, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Authors.buildAuthorUri(values.getAsString(Authors.AUTHOR_ID));
            }
            case POSTS: {
                db.insertOrThrow(Tables.POSTS, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Posts.buildPostUri(values.getAsString(Posts.POST_ID));
            }
            case POSTS_ID_TAGS: {
                db.insertOrThrow(Tables.POSTS_TAGS, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Tags.buildTagUri(values.getAsString(PostsTags.TAG_ID));
            }

            case POSTS_ID_COMMENTS: {
                db.insertOrThrow(Tables.POSTS_COMMENTS, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Comments.buildCommentUri(values.getAsString(PostsComments.COMMENT_ID));
            }
            case POSTS_ID_CATEGORIES: {
                db.insertOrThrow(Tables.POSTS_CATEGORIES, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Categories.buildCategoryUri(values.getAsString(PostsCategories.CATEGORY_ID));
            }
            case TAGS: {
                db.insertOrThrow(Tables.TAGS, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Tags.buildTagUri(values.getAsString(Tags.TAG_ID));
            }

            case COMMENTS: {
                db.insertOrThrow(Tables.COMMENTS, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Comments.buildCommentUri(values.getAsString(Comments.COMMENT_ID));
            }
            case SEARCH_SUGGEST: {
                db.insertOrThrow(Tables.SEARCH_SUGGEST, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return SearchSuggest.CONTENT_URI;
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (LOGV) Log.v(TAG, "delete(uri=" + uri + ")");
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final SelectionBuilder builder = buildSimpleSelection(uri);
        int retVal = builder.where(selection, selectionArgs).delete(db);
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (LOGV) Log.v(TAG, "update(uri=" + uri + ", values=" + values.toString() + ")");
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final SelectionBuilder builder = buildSimpleSelection(uri);
        int retVal = builder.where(selection, selectionArgs).update(db, values);
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    /**
     * Apply the given set of {@link ContentProviderOperation}, executing inside
     * a {@link SQLiteDatabase} transaction. All changes will be rolled back if
     * any single one fails.
     */
    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    /**
     * Build a simple {@link SelectionBuilder} to match the requested
     * {@link Uri}. This is usually enough to support {@link #insert},
     * {@link #update}, and {@link #delete} operations.
     */
    private SelectionBuilder buildSimpleSelection(Uri uri) {
        final SelectionBuilder builder = new SelectionBuilder();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case CATEGORIES: {
                return builder.table(Tables.CATEGORIES);
            }
            case CATEGORIES_ID: {
                final String categoryId = Categories.getCategoryId(uri);
                return builder.table(Tables.CATEGORIES)
                        .where(Categories.CATEGORY_ID + "=?", categoryId);
            }
            case AUTHORS: {
                return builder.table(Tables.AUTHORS);
            }
            case AUTHORS_ID: {
                final String authorId = Authors.getAuthorId(uri);
                return builder.table(Tables.AUTHORS)
                        .where(Authors.AUTHOR_ID + "=?", authorId);
            }
            case POSTS: {
                return builder.table(Tables.POSTS);
            }
            case POSTS_ID: {
                final String postId = Posts.getPostId(uri);
                return builder.table(Tables.POSTS)
                        .where(Posts.POST_ID + "=?", postId);
            }
            case POSTS_ID_TAGS: {
                final String postId = Posts.getPostId(uri);
                return builder.table(Tables.POSTS_TAGS)
                        .where(Posts.POST_ID + "=?", postId);
            }

            case POSTS_ID_COMMENTS: {
                final String postId = Posts.getPostId(uri);
                return builder.table(Tables.POSTS_COMMENTS)
                        .where(Posts.POST_ID + "=?", postId);
            }
            case POSTS_ID_CATEGORIES: {
                final String postId = Posts.getPostId(uri);
                return builder.table(Tables.POSTS_CATEGORIES)
                        .where(Posts.POST_ID + "=?", postId);
            }
            case TAGS: {
                return builder.table(Tables.TAGS);
            }
            case TAGS_ID: {
                final String tagId = Tags.getTagId(uri);
                return builder.table(Tables.TAGS)
                        .where(Tags.TAG_ID + "=?", tagId);
            }


            case COMMENTS: {
                return builder.table(Tables.COMMENTS);
            }
            case COMMENTS_ID: {
                final String commentId = Comments.getCommentId(uri);
                return builder.table(Tables.COMMENTS)
                        .where(Comments.COMMENT_ID + "=?", commentId);
            }

            case SEARCH_SUGGEST: {
                return builder.table(Tables.SEARCH_SUGGEST);
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }


    /**
     * Build an advanced {@link SelectionBuilder} to match the requested
     * {@link Uri}. This is usually only used by {@link #query}, since it
     * performs table joins useful for {@link Cursor} data.
     */
    private SelectionBuilder buildExpandedSelection(Uri uri, int match) {
        final SelectionBuilder builder = new SelectionBuilder();
        switch (match) {
            case CATEGORIES: {
                return builder.table(Tables.CATEGORIES)
                        .map(Categories.POSTS_COUNT, Subquery.CATEGORY_POSTS_COUNT);
            }
            case CATEGORIES_ID: {
                final String categoryId = Categories.getCategoryId(uri);
                return builder.table(Tables.CATEGORIES)
                        .where(Categories.CATEGORY_ID + "=?", categoryId);
            }
            case CATEGORIES_ID_POSTS: {
                final String categoryId = Categories.getCategoryId(uri);
                return builder.table(Tables.POSTS_CATEGORIES_JOIN_POSTS_AUTHORS)
                        .mapToTable(Posts._ID, Tables.POSTS)
                        .mapToTable(Posts.POST_ID, Tables.POSTS)
                        .mapToTable(Posts.AUTHOR_ID, Tables.POSTS)
                        .where(Qualified.POSTS_CATEGORIES_CATEGORY_ID + "=?", categoryId);
            }
            case AUTHORS: {
                return builder.table(Tables.AUTHORS);
            }
            case AUTHORS_ID: {
                final String authorId = Authors.getAuthorId(uri);
                return builder.table(Tables.AUTHORS)
                        .where(Authors.AUTHOR_ID + "=?", authorId);
            }
            case AUTHORS_ID_POSTS: {
                final String authorId = Authors.getAuthorId(uri);
                return builder.table(Tables.POSTS_JOIN_AUTHORS)
                        .mapToTable(Posts._ID, Tables.POSTS)
                        .mapToTable(Posts.AUTHOR_ID, Tables.POSTS)
                        .where(Qualified.POSTS_AUTHOR_ID + "=?", authorId);
            }
            case POSTS: {
                return builder.table(Tables.POSTS_JOIN_AUTHORS)
                        .mapToTable(Posts._ID, Tables.POSTS)
                        .mapToTable(Posts.AUTHOR_ID, Tables.POSTS);
            }
            case POSTS_SEARCH: {
                final String query = Posts.getSearchQuery(uri);
                return builder.table(Tables.POSTS_SEARCH_JOIN_POSTS_AUTHORS)
                        .map(Posts.SEARCH_SNIPPET, Subquery.POSTS_SNIPPET)
                        .mapToTable(Posts._ID, Tables.POSTS)
                        .mapToTable(Posts.POST_ID, Tables.POSTS)
                        .mapToTable(Posts.AUTHOR_ID, Tables.POSTS)
                        .where(PostsSearchColumns.BODY + " MATCH ?", query);
            }
            case POSTS_ID: {
                final String postId = Posts.getPostId(uri);
                return builder.table(Tables.POSTS_JOIN_AUTHORS)
                        .mapToTable(Posts._ID, Tables.POSTS)
                        .mapToTable(Posts.AUTHOR_ID, Tables.POSTS)
                        .where(Qualified.POSTS_POST_ID + "=?", postId);
            }
            case POSTS_ID_TAGS: {
                final String postId = Posts.getPostId(uri);
                return builder.table(Tables.POSTS_TAGS_JOIN_TAGS)
                        .mapToTable(Tags._ID, Tables.TAGS)
                        .mapToTable(Tags.TAG_ID, Tables.TAGS)
                        .where(Qualified.POSTS_TAGS_POST_ID + "=?", postId);
            }
            case POSTS_ID_CATEGORIES: {
                final String postId = Posts.getPostId(uri);
                return builder.table(Tables.POSTS_CATEGORIES_JOIN_CATEGORIES)
                        .mapToTable(Categories._ID, Tables.CATEGORIES)
                        .mapToTable(Categories.CATEGORY_ID, Tables.CATEGORIES)
                        .where(Qualified.POSTS_CATEGORIES_POST_ID + "=?", postId);
            }
            case POSTS_ID_COMMENTS: {
                final String postId = Posts.getPostId(uri);
                return builder.table(Tables.POSTS_COMMENTS_JOIN_COMMENTS)
                        .mapToTable(Comments._ID, Tables.COMMENTS)
                        .mapToTable(Comments.COMMENT_ID, Tables.COMMENTS)
                        .where(Qualified.POSTS_COMMENTS_POST_ID + "=?", postId);
            }
            case TAGS: {
                return builder.table(Tables.TAGS);
            }
            case TAGS_ID: {
                final String tagId = Tags.getTagId(uri);
                return builder.table(Tables.TAGS)
                        .where(Tags.TAG_ID + "=?", tagId);
            }
            case TAGS_ID_POSTS: {
                final String tagId = Tags.getTagId(uri);
                return builder.table(Tables.POSTS_TAGS_JOIN_POSTS_AUTHORS)
                        .mapToTable(Posts._ID, Tables.POSTS)
                        .mapToTable(Posts.POST_ID, Tables.POSTS)
                        .mapToTable(Posts.AUTHOR_ID, Tables.POSTS)
                        .where(Qualified.POSTS_TAGS_TAG_ID + "=?", tagId);
            }


            case COMMENTS: {
                return builder.table(Tables.COMMENTS);
            }
            case COMMENTS_ID: {
                final String commentId = Comments.getCommentId(uri);
                return builder.table(Tables.COMMENTS)
                        .where(Comments.COMMENT_ID + "=?", commentId);
            }
            case COMMENTS_ID_POSTS: {
                final String commentId = Comments.getCommentId(uri);
                return builder.table(Tables.POSTS_COMMENTS_JOIN_POSTS_AUTHORS)
                        .mapToTable(Posts._ID, Tables.POSTS)
                        .mapToTable(Posts.POST_ID, Tables.POSTS)
                        .mapToTable(Posts.AUTHOR_ID, Tables.POSTS)
                        .where(Qualified.POSTS_COMMENTS_COMMENT_ID + "=?", commentId);
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }

    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }

    private interface Subquery {

        String CATEGORY_POSTS_COUNT = "(SELECT COUNT(" + Qualified.POSTS_CATEGORIES_POST_ID
                + ") FROM " + Tables.POSTS_CATEGORIES + " WHERE "
                + Qualified.POSTS_CATEGORIES_CATEGORY_ID + "=" + Qualified.CATEGORIES_CATEGORY_ID + ")";
        String POSTS_SNIPPET = "snippet(" + Tables.POSTS_SEARCH + ",'{','}','\u2026')";
    }

    /**
     * {@link IfeveContract} fields that are fully qualified with a specific
     * parent {@link Tables}. Used when needed to work around SQL ambiguity.
     */
    private interface Qualified {
        String POSTS_POST_ID = Tables.POSTS + "." + Posts.POST_ID;
        String POSTS_AUTHOR_ID = Tables.POSTS + "." + Posts.AUTHOR_ID;

        String POSTS_CATEGORIES_POST_ID = Tables.POSTS_CATEGORIES + "."
                + PostsCategories.POST_ID;
        String POSTS_CATEGORIES_CATEGORY_ID = Tables.POSTS_CATEGORIES + "."
                + PostsCategories.CATEGORY_ID;

        String POSTS_TAGS_POST_ID = Tables.POSTS_TAGS + "."
                + PostsTags.POST_ID;
        String POSTS_TAGS_TAG_ID = Tables.POSTS_TAGS + "."
                + PostsTags.TAG_ID;


        String POSTS_COMMENTS_POST_ID = Tables.POSTS_COMMENTS + "."
                + PostsComments.POST_ID;
        String POSTS_COMMENTS_COMMENT_ID = Tables.POSTS_COMMENTS + "."
                + PostsComments.COMMENT_ID;


        String CATEGORIES_CATEGORY_ID = Tables.CATEGORIES + "." + Categories.CATEGORY_ID;
    }

}
