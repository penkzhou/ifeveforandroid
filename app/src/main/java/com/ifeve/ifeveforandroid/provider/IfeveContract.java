package com.ifeve.ifeveforandroid.provider;

import android.app.SearchManager;
import android.net.Uri;
import android.provider.BaseColumns;

import java.util.List;

/**
 * Created by penkzhou on 14-8-20.
 */
public class IfeveContract {

    /**
     * Special value for {@link SyncColumns#UPDATED} indicating that an entry
     * has never been updated, or doesn't exist yet.
     */
    public static final long UPDATED_NEVER = -2;

    /**
     * Special value for {@link SyncColumns#UPDATED} indicating that the last
     * update time is unknown, usually when inserted from a local file source.
     */
    public static final long UPDATED_UNKNOWN = -1;
    public static final String CONTENT_AUTHORITY = "com.ifeve.ifeveforandroid";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final int CATEGORY_LOADER = 0;
    public static final int POST_LOADER = 0;
    public static final int AUTHOR_LOADER = 0;
    public static final int TAG_LOADER = 0;
    public static final int COMMENT_LOADER = 0;
    private static final String PATH_POST = "posts";
    private static final String PATH_TAG = "tags";
    private static final String PATH_CATEGORY = "categories";
    private static final String PATH_AUTHOR = "authors";
    private static final String PATH_COMMENT = "comments";
    private static final String PATH_SEARCH = "search";
    private static final String PATH_SEARCH_SUGGEST = "search_suggest_query";

    private IfeveContract() {
    }

    public interface SyncColumns {
        /**
         * Last time this entry was updated or synchronized.
         */
        String UPDATED = "updated";
    }

    interface PostsColumns {

        /**
         * comment_count: 7
         * comment_status: "open"
         * comments: Array[7]
         * content:""
         * custom_fields: Object
         * date: "2014-07-26 12:55:02"
         * excerpt: ""
         * id: 13840
         * modified: "2014-07-29 00:05:50"
         * slug: "13840"
         * status: "publish"
         * tags: Array[0]
         * title: "原子循环计数器"
         * title_plain: "原子循环计数器"
         * type: "post"
         * url: "http://ifeve.com/13840/"
         */
        String POST_URL = "post_url";
        String POST_ID = "post_id";
        String POST_SLUG = "post_slug";
        String POST_STATUS = "post_status";
        String POST_CONTENT = "post_content";
        String POST_TYPE = "post_type";
        String POST_TITLE_PLAIN = "post_title_plain";
        String POST_TITLE = "post_title";
        String POST_EXCERPT = "post_excerpt";
        String POST_COMMENT_COUNT = "post_comment_count";
        String POST_COMMENT_STATUS = "post_comment_status";
        String POST_VIEW_COUNT = "post_view_count";
        String POST_CREATE_DATE = "post_create_date";
        String POST_MODIFY_DATE = "post_modify_date";
    }

    interface AuthorsColumns {
        /**
         * description: ""
         * first_name: ""
         * id: 2154
         * last_name: ""
         * name: "owenludong"
         * nickname: "owenludong"
         * slug: "owenludong"
         * url: "http://weibo.com/1884112540"
         */

        String AUTHOR_ID = "author_id";
        String AUTHOR_URL = "author_url";
        String AUTHOR_SLUG = "author_slug";
        String AUTHOR_NICKNAME = "author_nickname";
        String AUTHOR_NAME = "author_name";
        String AUTHOR_LAST_NAME = "author_last_name";
        String AUTHOR_FIRST_NAME = "author_first_name";
        String AUTHOR_DESC = "author_description";
    }

    interface CategoriesColumns {
        /**
         * "id":310,
         * "slug":"guava-2",
         * "title":"guava",
         * "description":"",
         * "parent":73,
         * "post_count":21
         */
        String CATEGORY_ID = "category_id";
        String CATEGORY_TITLE = "category_title";
        String CATEGORY_SLUG = "category_slug";
        String CATEGORY_PARENT = "category_parent";
        String CATEGORY_DESC = "category_description";
        String CATEGORY_POST_COUNT = "category_post_count";
    }


    interface CommentsColumns {
        /**
         * content: "<p>The link is inaccessible</p>↵"
         * date: "2014-07-26 22:16:14"
         * id: 22956
         * name: "eric"
         * parent: 0
         * url: ""
         */
        String COMMENT_ID = "comment_id";
        String COMMENT_DATE = "comment_DATE";
        String COMMENT_CONTENT = "comment_content";
        String COMMENT_PARENT = "comment_parent";
        String COMMENT_URL = "comment_url";
        String COMMENT_NAME = "comment_name";
    }


    interface TagsColumns {
        /**
         * "id":69
         * "slug":"java"
         * "title":"JAVA"
         * "description":""
         * "post_count":38"
         */
        String TAG_ID = "tag_id";
        String TAG_TITLE = "tag_title";
        String TAG_SLUG = "tag_slug";
        String TAG_DESC = "tag_description";
        String TAG_POST_COUNT = "tag_post_count";
    }


    public static class Posts implements PostsColumns, AuthorsColumns, SyncColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_POST).build();
        public static final String SEARCH_SNIPPET = "search_snippet";
        public static final String AUTHOR_ID = "author_id";
        /**
         * Default "ORDER BY" clause.
         */
        public static final String DEFAULT_SORT = PostsColumns.POST_CREATE_DATE + " DESC";

        /**
         * Build {@link Uri} for requested {@link #POST_ID}.
         */
        public static Uri buildPostUri(String postId) {
            return CONTENT_URI.buildUpon().appendPath(postId).build();
        }

        /**
         * Build {@link Uri} that references any {@link Categories} associated with
         * the requested {@link #POST_ID}.
         */
        public static Uri buildCategoriesDirUri(String postId) {
            return CONTENT_URI.buildUpon().appendPath(postId).appendPath(PATH_CATEGORY).build();
        }

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_POST;

        public static Uri buildCommentsDirUri(String postId) {
            return CONTENT_URI.buildUpon().appendPath(postId).appendPath(PATH_COMMENT).build();
        }

        public static Uri buildTagsAtDirUri(String postId) {
            return CONTENT_URI.buildUpon().appendPath(postId).appendPath(PATH_TAG).build();
        }

        public static Uri buildSearchUri(String query) {
            return CONTENT_URI.buildUpon().appendPath(PATH_SEARCH).appendPath(query).build();
        }

        public static boolean isSearchUri(Uri uri) {
            List<String> pathSegments = uri.getPathSegments();
            return pathSegments.size() >= 2 && PATH_SEARCH.equals(pathSegments.get(1));
        }

        /**
         * Read {@link #POST_ID} from {@link Posts} {@link Uri}.
         */
        public static String getPostId(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_POST;

        public static String getSearchQuery(Uri uri) {
            return uri.getPathSegments().get(2);
        }


        // TODO: shortcut primary category to offer sub-sorting here


    }

    public static class Categories implements CategoriesColumns, SyncColumns, BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CATEGORY).build();
        /**
         * Count of {@link Posts} inside given category.
         */
        public static final String POSTS_COUNT = "posts_count";
        /**
         * Default "ORDER BY" clause.
         */
        public static final String DEFAULT_SORT = CategoriesColumns.CATEGORY_TITLE + " ASC";
        /**
         * "All categorys" ID.
         */
        public static final String ALL_CATEGORY_ID = "all";

        /**
         * Build {@link Uri} for requested {@link #CATEGORY_ID}.
         */
        public static Uri buildCategoryUri(String categoryId) {
            return CONTENT_URI.buildUpon().appendPath(categoryId).build();
        }

        /**
         * Build {@link Uri} that references any {@link Posts} associated
         * with the requested {@link #CATEGORY_ID}.
         */
        public static Uri buildPostsUri(String categoryId) {
            return CONTENT_URI.buildUpon().appendPath(categoryId).appendPath(PATH_POST).build();
        }

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORY;

        /**
         * Read {@link #CATEGORY_ID} from {@link Categories} {@link Uri}.
         */
        public static String getCategoryId(Uri uri) {
            return uri.getPathSegments().get(1);
        }


        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORY;


    }


    /**
     * Authors are physical locations at the conference venue.
     */
    public static class Authors implements AuthorsColumns, BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_AUTHOR).build();
        /**
         * Default "ORDER BY" clause.
         */
        public static final String DEFAULT_SORT = AuthorsColumns.AUTHOR_NAME + " ASC";

        /**
         * Build {@link Uri} for requested {@link #AUTHOR_ID}.
         */
        public static Uri buildAuthorUri(String authorId) {
            return CONTENT_URI.buildUpon().appendPath(authorId).build();
        }

        /**
         * Build {@link Uri} that references any {@link Posts} associated
         * with the requested {@link #AUTHOR_ID}.
         */
        public static Uri buildPostsDirUri(String authorId) {
            return CONTENT_URI.buildUpon().appendPath(authorId).appendPath(PATH_POST).build();
        }

        /**
         * Read {@link #AUTHOR_ID} from {@link Authors} {@link Uri}.
         */
        public static String getAuthorId(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_AUTHOR;


        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_AUTHOR;


    }


    public static class Tags implements TagsColumns, SyncColumns, BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TAG).build();
        /**
         * Count of {@link Posts} inside given tag.
         */
        public static final String POSTS_COUNT = "posts_count";
        /**
         * Default "ORDER BY" clause.
         */
        public static final String DEFAULT_SORT = TagsColumns.TAG_TITLE + " ASC";

        /**
         * Build {@link Uri} for requested {@link #TAG_ID}.
         */
        public static Uri buildTagUri(String tagId) {
            return CONTENT_URI.buildUpon().appendPath(tagId).build();
        }

        /**
         * Build {@link Uri} that references any {@link Posts} associated
         * with the requested {@link #TAG_ID}.
         */
        public static Uri buildPostsUri(String tagId) {
            return CONTENT_URI.buildUpon().appendPath(tagId).appendPath(PATH_POST).build();
        }

        /**
         * Read {@link #TAG_ID} from {@link Tags} {@link Uri}.
         */
        public static String getTagId(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_TAG;


        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_TAG;


    }


    public static class Comments implements CommentsColumns, SyncColumns, BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COMMENT).build();
        /**
         * Count of {@link Posts} inside given comment.
         */
        public static final String POSTS_COUNT = "posts_count";
        /**
         * Default "ORDER BY" clause.
         */
        public static final String DEFAULT_SORT = CommentsColumns.COMMENT_DATE + " ASC";

        /**
         * Build {@link Uri} for requested {@link #COMMENT_ID}.
         */
        public static Uri buildCommentUri(String commentId) {
            return CONTENT_URI.buildUpon().appendPath(commentId).build();
        }

        /**
         * Build {@link Uri} that references any {@link Posts} associated
         * with the requested {@link #COMMENT_ID}.
         */
        public static Uri buildPostsUri(String commentId) {
            return CONTENT_URI.buildUpon().appendPath(commentId).appendPath(PATH_POST).build();
        }

        /**
         * Read {@link #COMMENT_ID} from {@link Comments} {@link Uri}.
         */
        public static String getCommentId(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_COMMENT;


        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_COMMENT;


    }


    public static class SearchSuggest {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SEARCH_SUGGEST).build();

        public static final String DEFAULT_SORT = SearchManager.SUGGEST_COLUMN_TEXT_1
                + " COLLATE NOCASE ASC";
    }


}
