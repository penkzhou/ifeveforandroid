package com.ifeve.ifeveforandroid.model;

/**
 * Created by penkzhou on 14-8-20.
 */
public class IfeveCategory {
    private int category_id;
    private String category_desc;
    private String category_slug;
    private String category_title;
    private int category_post_count;

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_desc() {
        return category_desc;
    }

    public void setCategory_desc(String category_desc) {
        this.category_desc = category_desc;
    }

    public String getCategory_slug() {
        return category_slug;
    }

    public void setCategory_slug(String category_slug) {
        this.category_slug = category_slug;
    }

    public String getCategory_title() {
        return category_title;
    }

    public void setCategory_title(String category_title) {
        this.category_title = category_title;
    }

    public int getCategory_post_count() {
        return category_post_count;
    }

    public void setCategory_post_count(int category_post_count) {
        this.category_post_count = category_post_count;
    }
}
