package com.ifeve.ifeveforandroid.model;

/**
 * Created by penkzhou on 14-8-20.
 */
public class IfevePost {
    private String author_name;
    private int author_id;
    private int comment_count;
    private String post_content;
    private String post_date;
    private String post_modified_date;
    private String post_slug;
    private String post_status;
    private String post_type;
    private String post_title;
    private String post_id;

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public int getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(int author_id) {
        this.author_id = author_id;
    }

    public int getComment_count() {
        return comment_count;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_modified_date() {
        return post_modified_date;
    }

    public void setPost_modified_date(String post_modified_date) {
        this.post_modified_date = post_modified_date;
    }

    public String getPost_slug() {
        return post_slug;
    }

    public void setPost_slug(String post_slug) {
        this.post_slug = post_slug;
    }

    public String getPost_status() {
        return post_status;
    }

    public void setPost_status(String post_status) {
        this.post_status = post_status;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }
}
