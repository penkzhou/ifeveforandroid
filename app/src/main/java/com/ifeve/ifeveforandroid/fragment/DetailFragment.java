package com.ifeve.ifeveforandroid.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ifeve.ifeveforandroid.AuthorDetailActivity;
import com.ifeve.ifeveforandroid.CategoryPostListActivity;
import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.TagPostListActivity;
import com.ifeve.ifeveforandroid.provider.IfeveContract;

/**
 * Created by penkzhou on 14-8-20.
 */
public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = DetailFragment.class.getSimpleName();
    private static final String AUTHOR_ID_KEY = "author_id";
    private static final int POST_LOADER = 0;
    private static final int CATEGORY_LOADER = 1;
    private static final int TAG_LOADER = 2;
    private static final int AUTHOR_LOADER = 3;


    private WebView webView;
    private LinearLayout tagListLayout;
    private LinearLayout categoryListLayout;
    private TextView postTitle;
    private TextView postCommentCount;
    private TextView postViewCount;
    private TextView postCreateTime;
    private TextView postAuthorName;
    private String postId;
    private String postUrl;
    private String shareString;
    private String authorId;
    private LayoutInflater layoutInflater;

    private ShareActionProvider mShareActionProvider;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        webView = (WebView) rootView.findViewById(R.id.postView);
        tagListLayout = (LinearLayout) rootView.findViewById(R.id.post_detail_tag);
        categoryListLayout = (LinearLayout) rootView.findViewById(R.id.post_detail_category);
        postTitle = (TextView) rootView.findViewById(R.id.post_detail_title);
        postCommentCount = (TextView) rootView.findViewById(R.id.post_detail_count);
        postViewCount = (TextView) rootView.findViewById(R.id.post_detail_view_count);
        postAuthorName = (TextView) rootView.findViewById(R.id.post_detail_author);
        postCreateTime = (TextView) rootView.findViewById(R.id.post_detail_date);
        layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Intent intent = getActivity().getIntent();
        postId = intent.getStringExtra("postId");
        return rootView;
    }

    public void setupAuthorClickListener() {
        postAuthorName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AuthorDetailActivity.class);
                intent.putExtra("authorId", authorId);
                startActivity(intent);
            }
        });
    }

    public void generateShareString() {
        shareString = postTitle.getText() + " " + postUrl + " #并发编程网# ";
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(POST_LOADER, null, this);
        getLoaderManager().initLoader(CATEGORY_LOADER, null, this);
        getLoaderManager().initLoader(TAG_LOADER, null, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_post_share) {
            Intent intent = getDefaultIntent();
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Fetch and store ShareActionProvider
        inflater.inflate(R.menu.post_detail, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.menu_post_share);

        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        mShareActionProvider.setShareIntent(getDefaultIntent());
        super.onCreateOptionsMenu(menu, inflater);
    }

    private Intent getDefaultIntent() {
        generateShareString();
        Log.i("DetailFragment", "getDefaultIntent' share string is " + shareString);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.putExtra(Intent.EXTRA_TEXT, shareString);
        return intent;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.i(TAG, "create loader for " + i);
        Uri uri = null;
        switch (i) {
            case POST_LOADER:
                uri = IfeveContract.Posts.buildPostUri(postId);
                return new CursorLoader(getActivity(), uri,
                        null,
                        null, null,
                        IfeveContract.Posts.DEFAULT_SORT);
            case CATEGORY_LOADER:
                uri = IfeveContract.Posts.buildCategoriesDirUri(postId);
                return new CursorLoader(getActivity(), uri,
                        null,
                        null, null,
                        IfeveContract.Categories.DEFAULT_SORT);
            case TAG_LOADER:
                uri = IfeveContract.Posts.buildTagsAtDirUri(postId);
                return new CursorLoader(getActivity(), uri,
                        null,
                        null, null,
                        IfeveContract.Tags.DEFAULT_SORT);
            case AUTHOR_LOADER:
                authorId = bundle.getString(AUTHOR_ID_KEY);
                uri = IfeveContract.Authors.buildAuthorUri(authorId);
                return new CursorLoader(getActivity(), uri,
                        null,
                        null, null,
                        IfeveContract.Authors.DEFAULT_SORT);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (!cursor.moveToFirst()) {
            return;
        }
        switch (cursorLoader.getId()) {
            case POST_LOADER:
                Log.v(TAG, "In onLoadFinished for POST_LOADER");
                onPostLoadFinished(cursor);
                break;
            case CATEGORY_LOADER:
                Log.v(TAG, "In onLoadFinished for CATEGORY_LOADER");
                onCategoryLoadFinished(cursor);
                break;
            case TAG_LOADER:
                Log.v(TAG, "In onLoadFinished for TAG_LOADER");
                onTagLoadFinished(cursor);
                break;
            case AUTHOR_LOADER:
                Log.v(TAG, "In onLoadFinished for AUTHOR_LOADER");
                onAuthorLoadFinished(cursor);
                break;
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    public void onPostLoadFinished(Cursor cursor) {


        /**
         * set the author id after get the whole post info.
         */
        String authorId = cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.AUTHOR_ID));
        Bundle bundle = new Bundle();
        bundle.putString(AUTHOR_ID_KEY, authorId);
        getLoaderManager().initLoader(AUTHOR_LOADER, bundle, this);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        String viewContent = cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_CONTENT));
        String post_title = cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_TITLE));
        String post_date = cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_CREATE_DATE));
        post_date = post_date.split(" ")[0];
        String post_comment_count = cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_COMMENT_COUNT));
        String post_view_count = cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_VIEW_COUNT));
        postUrl = cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_URL));
        postTitle.setText(post_title);
        postCreateTime.setText(post_date);
        postCommentCount.setText(post_comment_count);
        postViewCount.setText(post_view_count);
        viewContent = viewContent.replaceAll("\r\n", "<br/>");
        webView.loadData(viewContent, "text/html; charset=UTF-8", null);
        webView.setVisibility(View.VISIBLE);
    }

    public void onTagLoadFinished(Cursor cursor) {
        if (cursor.getCount() == 0) return;
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        tagListLayout.removeAllViews();
        int maxWidth = display.getWidth() - 20;

        LinearLayout.LayoutParams params;
        LinearLayout newLL = new LinearLayout(getActivity());
        newLL.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        newLL.setGravity(Gravity.LEFT);
        newLL.setOrientation(LinearLayout.HORIZONTAL);

        int widthSoFar = 0;

        if (cursor.getCount() > 0) {
            LinearLayout tagHeader = (LinearLayout) layoutInflater.inflate(R.layout.tag_header_view, null);
            LinearLayout LL = new LinearLayout(getActivity());
            LL.setOrientation(LinearLayout.HORIZONTAL);
            LL.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
            LL.setLayoutParams(new ListView.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            tagHeader.measure(0, 0);
            params = new LinearLayout.LayoutParams(tagHeader.getMeasuredWidth(),
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            LL.addView(tagHeader, params);
            LL.measure(0, 0);
            newLL.addView(LL);
        }


        do {
            LinearLayout tagItemView = (LinearLayout) layoutInflater.inflate(R.layout.tag_view, null);
            TextView tagView = (TextView) tagItemView.findViewById(R.id.tag_text);
            final String tagTitle = cursor.getString(cursor.getColumnIndex(IfeveContract.Tags.TAG_TITLE));
            String tagId = cursor.getString(cursor.getColumnIndex(IfeveContract.Tags.TAG_ID));
            tagView.setText(tagTitle);
            tagView.setTag(tagId);
            tagView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String clickTagId = (String) v.getTag();
                    Intent intent = new Intent(getActivity(), TagPostListActivity.class);
                    intent.putExtra(TagPostListActivity.TAG_ID, clickTagId);
                    intent.putExtra(TagPostListActivity.TAG_TITLE, ((TextView) v).getText());
                    startActivity(intent);
                }
            });

            LinearLayout LL = new LinearLayout(getActivity());
            LL.setOrientation(LinearLayout.HORIZONTAL);
            LL.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
            LL.setLayoutParams(new ListView.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            tagItemView.measure(0, 0);
            params = new LinearLayout.LayoutParams(tagItemView.getMeasuredWidth(),
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            LL.addView(tagItemView, params);
            LL.measure(0, 0);
            widthSoFar += tagItemView.getMeasuredWidth();// YOU MAY NEED TO ADD THE MARGINS
            if (widthSoFar >= maxWidth) {
                tagListLayout.addView(newLL);

                newLL = new LinearLayout(getActivity());
                newLL.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                newLL.setOrientation(LinearLayout.HORIZONTAL);
                newLL.setGravity(Gravity.LEFT);
                params = new LinearLayout.LayoutParams(LL
                        .getMeasuredWidth(), LL.getMeasuredHeight());
                newLL.addView(LL, params);
                widthSoFar = LL.getMeasuredWidth();
            } else {
                newLL.addView(LL);
            }
        } while (cursor.moveToNext());
        tagListLayout.addView(newLL);
        tagListLayout.setVisibility(View.VISIBLE);
    }

    public void onCategoryLoadFinished(Cursor cursor) {
        if (cursor.getCount() == 0) return;
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        categoryListLayout.removeAllViews();
        int maxWidth = display.getWidth() - 20;

        LinearLayout.LayoutParams params;
        LinearLayout newLL = new LinearLayout(getActivity());
        newLL.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        newLL.setGravity(Gravity.LEFT);
        newLL.setOrientation(LinearLayout.HORIZONTAL);
        if (cursor.getCount() > 0) {
            LinearLayout categoryHeader = (LinearLayout) layoutInflater.inflate(R.layout.category_header_view, null);
            LinearLayout LL = new LinearLayout(getActivity());
            LL.setOrientation(LinearLayout.HORIZONTAL);
            LL.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
            LL.setLayoutParams(new ListView.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            categoryHeader.measure(0, 0);
            params = new LinearLayout.LayoutParams(categoryHeader.getMeasuredWidth(),
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            LL.addView(categoryHeader, params);
            LL.measure(0, 0);
            newLL.addView(LL);
        }

        int widthSoFar = 0;
        do {

            LinearLayout categoryItemView = (LinearLayout) layoutInflater.inflate(R.layout.category_view, null);
            TextView categoryView = (TextView) categoryItemView.findViewById(R.id.category_text);
            String categoryTitle = cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_TITLE));
            String categoryId = cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_ID));
            categoryView.setText(categoryTitle);
            categoryView.setTag(categoryId);
            //categoryListLayout.addView(categoryItemView);

            categoryView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String clickCategoryId = (String) v.getTag();
                    Intent intent = new Intent(getActivity(), CategoryPostListActivity.class);
                    intent.putExtra(CategoryPostListActivity.CATEGORY_ID, clickCategoryId);
                    intent.putExtra(CategoryPostListActivity.CATEGORY_TITLE, ((TextView) v).getText());
                    startActivity(intent);
                }
            });

            LinearLayout LL = new LinearLayout(getActivity());
            LL.setOrientation(LinearLayout.HORIZONTAL);
            LL.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
            LL.setLayoutParams(new ListView.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            categoryItemView.measure(0, 0);
            params = new LinearLayout.LayoutParams(categoryItemView.getMeasuredWidth(),
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            LL.addView(categoryItemView, params);
            LL.measure(0, 0);
            widthSoFar += categoryItemView.getMeasuredWidth();// YOU MAY NEED TO ADD THE MARGINS
            if (widthSoFar >= maxWidth) {
                categoryListLayout.addView(newLL);

                newLL = new LinearLayout(getActivity());
                newLL.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                newLL.setOrientation(LinearLayout.HORIZONTAL);
                newLL.setGravity(Gravity.LEFT);
                params = new LinearLayout.LayoutParams(LL
                        .getMeasuredWidth(), LL.getMeasuredHeight());
                newLL.addView(LL, params);
                widthSoFar = LL.getMeasuredWidth();
            } else {
                newLL.addView(LL);
            }
        } while (cursor.moveToNext());
        categoryListLayout.addView(newLL);
        categoryListLayout.setVisibility(View.VISIBLE);

    }

    public void onAuthorLoadFinished(Cursor cursor) {
        String authorName = cursor.getString(cursor.getColumnIndex(IfeveContract.Authors.AUTHOR_NAME));
        postAuthorName.setText(authorName);
        setupAuthorClickListener();
    }

}
