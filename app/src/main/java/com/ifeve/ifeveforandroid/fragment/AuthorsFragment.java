package com.ifeve.ifeveforandroid.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ifeve.ifeveforandroid.CategoryPostListActivity;
import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.adapter.CategoryListCursorAdapter;
import com.ifeve.ifeveforandroid.provider.IfeveContract;

/**
 * Created by penkzhou on 14-8-24.
 */
public class AuthorsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private ListView authorList;
    private CategoryListCursorAdapter authorListCursorAdapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_categories, container, false);
        authorList = (ListView) rootView.findViewById(R.id.categoriesList);
        authorListCursorAdapter = new CategoryListCursorAdapter(getActivity(), null, 0);
        authorList.setAdapter(authorListCursorAdapter);
        authorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = authorListCursorAdapter.getCursor();
                if (cursor != null && cursor.moveToPosition(position)) {
                    Intent intent = new Intent(getActivity(), CategoryPostListActivity.class);
                    String authorId = cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_ID));
                    String authorTitle = cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_TITLE));
                    intent.putExtra(CategoryPostListActivity.CATEGORY_ID, authorId);
                    intent.putExtra(CategoryPostListActivity.CATEGORY_TITLE, authorTitle);
                    startActivity(intent);
                }
            }
        });
        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        Uri baseUri = IfeveContract.Categories.CONTENT_URI;

        return new CursorLoader(getActivity(), baseUri,
                null,
                null, null,
                IfeveContract.Categories.DEFAULT_SORT);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        authorListCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        authorListCursorAdapter.swapCursor(null);
    }
}
