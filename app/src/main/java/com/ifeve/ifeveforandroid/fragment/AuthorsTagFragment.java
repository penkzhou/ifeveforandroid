package com.ifeve.ifeveforandroid.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ifeve.ifeveforandroid.AuthorDetailActivity;
import com.ifeve.ifeveforandroid.MainActivity;
import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.service.ServiceHelper;
import com.ifeve.ifeveforandroid.service.SyncService;
import com.ifeve.ifeveforandroid.util.IfeveAPI;
import com.ifeve.ifeveforandroid.util.MyResultReceiver;

/**
 * Created by penkzhou on 14-8-24.
 */
public class AuthorsTagFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener, MyResultReceiver.Receiver {
    private static final String TAG = AuthorsTagFragment.class.getSimpleName();
    private LinearLayout authorList;
    private SwipeRefreshLayout authorTagScrollView;
    private LayoutInflater layoutInflater;
    private MyResultReceiver myResultReceiver;
    private Uri buildUri;
    private int token;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_author_tag, container, false);
        authorList = (LinearLayout) rootView.findViewById(R.id.author_tag);
        authorTagScrollView = (SwipeRefreshLayout) rootView.findViewById(R.id.authorTagScrollView);
        authorTagScrollView.setOnRefreshListener(this);
        layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        Uri baseUri = IfeveContract.Authors.CONTENT_URI;

        return new CursorLoader(getActivity(), baseUri,
                null,
                null, null,
                IfeveContract.Authors.DEFAULT_SORT);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (!cursor.moveToFirst()) return;

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        authorList.removeAllViews();
        int maxWidth = display.getWidth() - 20;

        LinearLayout.LayoutParams params;
        LinearLayout newLL = new LinearLayout(getActivity());
        newLL.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        newLL.setGravity(Gravity.LEFT);
        newLL.setOrientation(LinearLayout.HORIZONTAL);

        int widthSoFar = 0;
        do {
            LinearLayout authorItemView = (LinearLayout) layoutInflater.inflate(R.layout.author_view, null);
            TextView authorView = (TextView) authorItemView.findViewById(R.id.author_text);
            String authorTitle = cursor.getString(cursor.getColumnIndex(IfeveContract.Authors.AUTHOR_NAME));
            String authorId = cursor.getString(cursor.getColumnIndex(IfeveContract.Authors.AUTHOR_ID));
            authorView.setText(authorTitle);
            authorView.setTag(authorId);
            authorView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String clickAuthorId = (String) v.getTag();
                    Intent intent = new Intent(getActivity(), AuthorDetailActivity.class);
                    intent.putExtra("authorId", clickAuthorId);
                    startActivity(intent);
                }
            });

            LinearLayout LL = new LinearLayout(getActivity());
            LL.setOrientation(LinearLayout.HORIZONTAL);
            LL.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
            LL.setLayoutParams(new ListView.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            authorItemView.measure(0, 0);
            params = new LinearLayout.LayoutParams(authorItemView.getMeasuredWidth(),
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            LL.addView(authorItemView, params);
            LL.measure(0, 0);
            widthSoFar += authorItemView.getMeasuredWidth();// YOU MAY NEED TO ADD THE MARGINS
            if (widthSoFar >= maxWidth) {
                authorList.addView(newLL);

                newLL = new LinearLayout(getActivity());
                newLL.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                newLL.setOrientation(LinearLayout.HORIZONTAL);
                newLL.setGravity(Gravity.LEFT);
                params = new LinearLayout.LayoutParams(LL
                        .getMeasuredWidth(), LL.getMeasuredHeight());
                newLL.addView(LL, params);
                widthSoFar = LL.getMeasuredWidth();
            } else {
                newLL.addView(LL);
            }
        } while (cursor.moveToNext());
        authorList.addView(newLL);
        authorList.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        Log.d(TAG, "onReceiveResult(resultCode=" + resultCode + ", resultData="
                + resultData.toString());

        switch (resultCode) {
            case SyncService.STATUS_RUNNING:


                break;
            case SyncService.STATUS_FINISHED:
                Toast.makeText(getActivity(), "成功获取所有作者列表。", Toast.LENGTH_LONG).show();
                getLoaderManager().restartLoader(IfeveContract.AUTHOR_LOADER, null, this);
                authorTagScrollView.setRefreshing(false);
                break;
            case SyncService.STATUS_ERROR:
                Toast.makeText(getActivity(), "在获取作者列表的时候出现异常，请稍后再试。", Toast.LENGTH_LONG).show();
                break;

        }
    }

    @Override
    public void registerReceiver() {
        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);
    }

    private void loadData() {

        Log.d(TAG, "loadData() called");
        token = MainActivity.AUTHOR_TOKEN;
        registerReceiver();
        //get the recent post
        buildUri = Uri.parse(IfeveAPI.GET_ALL_AUTHORS);
        Log.d(TAG, "loadData() ---from new--- for url : " + buildUri.toString());
        ServiceHelper.execute(getActivity(), myResultReceiver, token, buildUri.toString());
    }

    @Override
    public void onRefresh() {
        loadData();
    }
}
