package com.ifeve.ifeveforandroid.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ifeve.ifeveforandroid.CategoryPostListActivity;
import com.ifeve.ifeveforandroid.DetailActivity;
import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.adapter.PostListCursorAdapter;
import com.ifeve.ifeveforandroid.provider.IfeveContract;


/**
 * Created by penkzhou on 14-8-13.
 */
public class CategoryPostListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "CategoryPostListFragment";

    private String categoryId;
    private String categoryTitle;
    private ListView postListView;
    private PostListCursorAdapter postListCursorAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_normal_postlist, container, false);
        Intent intent = getActivity().getIntent();
        categoryId = intent.getStringExtra(CategoryPostListActivity.CATEGORY_ID);
        categoryTitle = intent.getStringExtra(CategoryPostListActivity.CATEGORY_TITLE);
        getActivity().setTitle(String.format(getString(R.string.category_postlist_title), categoryTitle));
        postListCursorAdapter = new PostListCursorAdapter(getActivity(), null, 0);
        postListView = (ListView) rootView.findViewById(R.id.listview_post);
        postListView.setAdapter(postListCursorAdapter);
        postListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = postListCursorAdapter.getCursor();
                if (cursor != null && cursor.moveToPosition(position)) {
                    Intent intent = new Intent(getActivity(), DetailActivity.class)
                            .putExtra("postId", cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_ID)));
                    startActivity(intent);
                }
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        Uri baseUri = IfeveContract.Categories.buildPostsUri(categoryId);

        return new CursorLoader(getActivity(), baseUri,
                null,
                null, null,
                IfeveContract.Posts.DEFAULT_SORT);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        postListCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        postListCursorAdapter.swapCursor(null);

    }


}
