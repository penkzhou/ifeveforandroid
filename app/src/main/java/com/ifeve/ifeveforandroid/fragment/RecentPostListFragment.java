package com.ifeve.ifeveforandroid.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ifeve.ifeveforandroid.DetailActivity;
import com.ifeve.ifeveforandroid.MainActivity;
import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.adapter.PostListCursorAdapter;
import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.provider.IfeveProvider;
import com.ifeve.ifeveforandroid.service.ServiceHelper;
import com.ifeve.ifeveforandroid.service.SyncService;
import com.ifeve.ifeveforandroid.util.IfeveAPI;
import com.ifeve.ifeveforandroid.util.MyResultReceiver;

/**
 * Created by penkzhou on 14-8-13.
 */
public class RecentPostListFragment extends Fragment implements MyResultReceiver.Receiver, LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "RecentPostListFragment";
    private final static String KEY_RECENT_POST = "recent_post";
    private final static String RECENT_POST_SORT = "post_create_date DESC";
    private SharedPreferences prefs;
    private boolean first_time;
    private ListView postListView;
    private SwipeRefreshLayout postListLayout;
    private PostListCursorAdapter postListCursorAdapter;
    private MyResultReceiver myResultReceiver;
    private int token;
    private Uri buildUri;
    private boolean isRefresh = false;
    private int preLast;

    /**
     * @return a new instance of {@link com.ifeve.ifeveforandroid.fragment.RecentPostListFragment}, adding the parameters into a bundle and
     * setting them as arguments.
     */


    private void loadData() {

        Log.d(TAG, "loadData() called");
        token = MainActivity.POST_TOKEN;
        registerReceiver();
        //get the recent post
        buildUri = Uri.parse(IfeveAPI.GET_RECENT_POSTS);
        Log.d(TAG, "loadData() ---from new--- for url : " + buildUri.toString());
        ServiceHelper.execute(getActivity(), myResultReceiver, token, buildUri.toString());
    }


    private void loadData(int page) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_recent_postlist, container, false);
        postListCursorAdapter = new PostListCursorAdapter(getActivity(), null, 0);
        postListView = (ListView) rootView.findViewById(R.id.listview_post);
        postListLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.postListLayout);
        postListView.setAdapter(postListCursorAdapter);
        postListLayout.setOnRefreshListener(this);
//        postListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {
//            @Override
//            public void onLastItemVisible() {
//                Log.i("RecentPostListFragment", "last onononon");
//            }
//        });
        postListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = postListCursorAdapter.getCursor();
                int headerCount = postListView.getHeaderViewsCount();
                if (cursor != null && cursor.moveToPosition(position - headerCount)) {
                    Intent intent = new Intent(getActivity(), DetailActivity.class)
                            .putExtra("postId", cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_ID)));
                    startActivity(intent);
                }
            }
        });
        postListLayout.setRefreshing(false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreated");
    }


    @Override
    public void onStart() {
        super.onStart();
        first_time = prefs.getBoolean(KEY_RECENT_POST, true);
        Log.i(TAG, "onStart  recent post first time : " + first_time);
        if (first_time) {
            loadData();
        } else {
            getLoaderManager().initLoader(IfeveContract.POST_LOADER, null, this);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated");
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        Log.d(TAG, "onReceiveResult(resultCode=" + resultCode + ", resultData="
                + resultData.toString());

        switch (resultCode) {
            case SyncService.STATUS_RUNNING:


                break;
            case SyncService.STATUS_FINISHED:
                Toast.makeText(getActivity(), "成功获取所有文章列表。", Toast.LENGTH_LONG).show();
                if (isRefresh) {
                    getLoaderManager().restartLoader(IfeveContract.POST_LOADER, null, this);
                    postListLayout.setRefreshing(false);
                    isRefresh = false;
                }
                if (prefs.getBoolean(KEY_RECENT_POST, true)) {
                    getLoaderManager().initLoader(IfeveContract.POST_LOADER, null, this);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean(KEY_RECENT_POST, false);
                    editor.commit();
                }
                break;
            case SyncService.STATUS_ERROR:
                Toast.makeText(getActivity(), "在获取文章列表的时候出现异常，请稍后再试。", Toast.LENGTH_LONG).show();
                break;

        }
    }

    @Override
    public void registerReceiver() {
        Log.i(TAG, "registerReceiver");
        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);

    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.i(TAG, "onCreateLoader");
        Uri baseUri = IfeveContract.Posts.CONTENT_URI;
        baseUri = baseUri.buildUpon().appendQueryParameter(IfeveProvider.QUERY_PARAMETER_LIMIT, "20").build();
        return new CursorLoader(getActivity(), baseUri,
                null,
                null, null,
                RECENT_POST_SORT);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG, "onLoadFinished");
        postListCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        postListCursorAdapter.swapCursor(null);

    }


    @Override
    public void onRefresh() {
        isRefresh = true;
        loadData();
    }
}
