package com.ifeve.ifeveforandroid.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ifeve.ifeveforandroid.DetailActivity;
import com.ifeve.ifeveforandroid.MainActivity;
import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.adapter.PostListCursorAdapter;
import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.service.ServiceHelper;
import com.ifeve.ifeveforandroid.service.SyncService;
import com.ifeve.ifeveforandroid.util.IfeveAPI;
import com.ifeve.ifeveforandroid.util.MyResultReceiver;
import com.ifeve.ifeveforandroid.util.NormalUtil;
import com.ifeve.ifeveforandroid.view.LoadingFooter;

/**
 * Created by penkzhou on 14-8-13.
 */
public class PostListFragment extends Fragment implements MyResultReceiver.Receiver, LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "PostListFragment";
    private final static String KEY_CATEGORY_STRING = "slug";
    private SharedPreferences prefs;
    private boolean first_time;
    private String category;
    private String post_sort_order;
    private ListView postListView;
    private SwipeRefreshLayout postListLayout;
    private PostListCursorAdapter postListCursorAdapter;
    private int mPage = 1;
    private MyResultReceiver myResultReceiver;
    private int token;
    private Uri buildUri;
    private boolean isRefresh = false;
    private boolean load_more = false;
    private LoadingFooter mLoadingFooter;
    private int preLast;

    /**
     * @return a new instance of {@link PostListFragment}, adding the parameters into a bundle and
     * setting them as arguments.
     */
    public static PostListFragment newInstance(String category) {
        Log.i(TAG, "newInstance");
        Bundle bundle = new Bundle();
        bundle.putString(KEY_CATEGORY_STRING, category);
        PostListFragment fragment = new PostListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void loadData() {
        if (!NormalUtil.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "网络连接错误，无法刷新", Toast.LENGTH_SHORT).show();
            postListLayout.setRefreshing(false);
            mLoadingFooter.setState(LoadingFooter.State.Idle);
            return;
        }

        Log.d(TAG, "loadData() called");
        if (mPage == 1) {
            token = MainActivity.REFRESH_POST_CLEAN_TOKEN;
        } else {
            token = MainActivity.REFRESH_POST_TOKEN;
        }
        registerReceiver();
        buildUri = Uri.parse(IfeveAPI.GET_POSTS_BY_CATEGORY).buildUpon()
                .appendQueryParameter(IfeveAPI.ID_PARAM, category)
                .appendQueryParameter(IfeveAPI.PAGE_PARAM, mPage + "")
                .build();
        Log.d(TAG, "loadData() ---from new--- for url : " + buildUri.toString());
        ServiceHelper.execute(getActivity(), myResultReceiver, token, buildUri.toString());
    }


    private void loadData(int page) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        if (post_sort_order != null && !post_sort_order.equals(NormalUtil.getPreferredPostSortOrder(getActivity()))) {
            getLoaderManager().restartLoader(IfeveContract.POST_LOADER, null, this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        Bundle args = getArguments();
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (args != null) {
            this.category = args.getString(KEY_CATEGORY_STRING);
            Log.i(TAG, "onCreateView  categoryId : " + category);
        }
        View rootView = inflater.inflate(R.layout.fragment_postlist, container, false);
        postListCursorAdapter = new PostListCursorAdapter(getActivity(), null, 0);
        postListView = (ListView) rootView.findViewById(R.id.listview_post);
        postListLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.postListLayout);
        mLoadingFooter = new LoadingFooter(getActivity());
        postListView.addFooterView(mLoadingFooter.getView());
        postListView.setAdapter(postListCursorAdapter);
        postListLayout.setOnRefreshListener(this);
        postListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = postListCursorAdapter.getCursor();
                int headerCount = postListView.getHeaderViewsCount();
                if (cursor != null && cursor.moveToPosition(position - headerCount)) {
                    Intent intent = new Intent(getActivity(), DetailActivity.class)
                            .putExtra("postId", cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_ID)));
                    startActivity(intent);
                }
            }
        });

        postListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (mLoadingFooter.getState() == LoadingFooter.State.Loading
//                        || mLoadingFooter.getState() == LoadingFooter.State.TheEnd) {
//                    return;
//                }
//                if (postListCursorAdapter.getCount() < 20){
//                    mLoadingFooter.setState(LoadingFooter.State.TheEnd);
//                }
                //Toast.makeText(getActivity(),""+postListCursorAdapter.getCount(),Toast.LENGTH_SHORT).show();
                if (firstVisibleItem + visibleItemCount >= totalItemCount
                        && totalItemCount != 0
                        && totalItemCount != postListView.getHeaderViewsCount()
                        + postListView.getFooterViewsCount() && postListCursorAdapter.getCount() >= 20) {
                    loadNextPage();
                } else {
                    mLoadingFooter.setState(LoadingFooter.State.TheEnd);
                }

//                if (postListView.getRefreshableView().getLastVisiblePosition() == postListView.getRefreshableView().getAdapter().getCount() - 1
//                        && postListView.getRefreshableView().getChildAt(postListView.getRefreshableView().getChildCount() - 1).getBottom() <= postListView.getRefreshableView().getHeight()) {
//                    //Toast.makeText(getActivity(),category + "类别到底啦222",Toast.LENGTH_SHORT).show();
//
//                }
            }
        });

        return rootView;
    }

    private void loadNextPage() {
        isRefresh = true;
        load_more = true;
        mLoadingFooter.setState(LoadingFooter.State.Loading);
        mPage++;
        loadData();
        Log.i(TAG, "loadNextPage");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreated");
    }


    @Override
    public void onStart() {
        super.onStart();
        first_time = prefs.getBoolean(category, true);
        Log.i(TAG, "onStart  categoryId : " + category + " first time : " + first_time);
        if (first_time) {
            loadData();
        } else {
            getLoaderManager().initLoader(IfeveContract.POST_LOADER, null, this);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated");

        Bundle args = getArguments();

        if (args != null) {
            this.category = args.getString(KEY_CATEGORY_STRING);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        Log.d(TAG, "onReceiveResult(resultCode=" + resultCode + ", resultData="
                + resultData.toString());

        switch (resultCode) {
            case SyncService.STATUS_RUNNING:


                break;
            case SyncService.STATUS_FINISHED:
                if (isRefresh) {
                    getLoaderManager().restartLoader(IfeveContract.POST_LOADER, null, this);
                    if (load_more) {
                        Toast.makeText(getActivity(), "成功获取加载更多文章。", Toast.LENGTH_LONG).show();
                        mLoadingFooter.setState(LoadingFooter.State.Idle);
                    } else {
                        Toast.makeText(getActivity(), "成功获取所有文章列表。", Toast.LENGTH_LONG).show();
                        postListLayout.setRefreshing(false);
                    }
                    isRefresh = false;
                }
                if (prefs.getBoolean(category, true)) {
                    getLoaderManager().initLoader(IfeveContract.POST_LOADER, null, this);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean(category, false);
                    editor.commit();
                }

                Log.i(TAG, "onReceiveResult  categoryId : " + category);
                break;
            case SyncService.STATUS_ERROR:
                Toast.makeText(getActivity(), "在获取文章列表的时候出现异常，请稍后再试。", Toast.LENGTH_LONG).show();
                break;

        }
    }

    @Override
    public void registerReceiver() {
        Log.i(TAG, "registerReceiver");
        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);

    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.i(TAG, "onCreateLoader");
        Uri baseUri = IfeveContract.Categories.buildPostsUri(category);
        Uri searchUri = IfeveContract.Posts.buildSearchUri(category);

        post_sort_order = NormalUtil.getPreferredPostSortOrder(getActivity());
        return new CursorLoader(getActivity(), baseUri,
                null,
                null, null,
                post_sort_order);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG, "onLoadFinished");
        postListCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        postListCursorAdapter.swapCursor(null);

    }


    @Override
    public void onRefresh() {
        isRefresh = true;
        loadData();
    }
}
