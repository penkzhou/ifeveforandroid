package com.ifeve.ifeveforandroid.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ifeve.ifeveforandroid.DetailActivity;
import com.ifeve.ifeveforandroid.MainActivity;
import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.adapter.PostListCursorAdapter;
import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.provider.IfeveProvider;
import com.ifeve.ifeveforandroid.service.ServiceHelper;
import com.ifeve.ifeveforandroid.service.SyncService;
import com.ifeve.ifeveforandroid.util.IfeveAPI;
import com.ifeve.ifeveforandroid.util.MyResultReceiver;
import com.ifeve.ifeveforandroid.util.NormalUtil;
import com.ifeve.ifeveforandroid.view.MySwipeRefreshLayout;

/**
 * Created by penkzhou on 14-8-13.
 */
public class PostListFragment2 extends Fragment implements MyResultReceiver.Receiver, LoaderManager.LoaderCallbacks<Cursor>, MySwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener {
    private static final String TAG = "PostListFragment";
    private final static String KEY_CATEGORY_STRING = "slug";
    private SharedPreferences prefs;
    private boolean first_time;
    private String category;
    private String post_sort_order;
    private ListView postListView;
    private PostListCursorAdapter postListCursorAdapter;
    private int mPage = 1;
    private MyResultReceiver myResultReceiver;
    private MySwipeRefreshLayout mySwipeRefreshLayout;
    private int fragment_first_time;
    private int token;
    private Uri buildUri;
    private boolean isRefresh = false;
    private int preLast;

    /**
     * @return a new instance of {@link com.ifeve.ifeveforandroid.fragment.PostListFragment2}, adding the parameters into a bundle and
     * setting them as arguments.
     */
    public static PostListFragment2 newInstance(String category) {
        Log.i(TAG, "newInstance");
        Bundle bundle = new Bundle();
        bundle.putString(KEY_CATEGORY_STRING, category);
        PostListFragment2 fragment = new PostListFragment2();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void loadData() {

        Log.d(TAG, "loadData() called");
        if (mPage == 1) {
            token = MainActivity.POST_CLEAN_TOKEN;
        } else {
            token = MainActivity.POST_TOKEN;
        }
        registerReceiver();
        //get the recent post
        if ("index".equals(category)) {
            buildUri = Uri.parse(IfeveAPI.GET_RECENT_POSTS);
        } else {
            buildUri = Uri.parse(IfeveAPI.GET_POSTS_BY_CATEGORY_ID).buildUpon()
                    .appendQueryParameter(IfeveAPI.ID_PARAM, category)
                    .build();
        }
        Log.d(TAG, "loadData() ---from new--- for url : " + buildUri.toString());
        ServiceHelper.execute(getActivity(), myResultReceiver, token, buildUri.toString());
    }


    private void loadData(int page) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        if (post_sort_order != null && !post_sort_order.equals(NormalUtil.getPreferredPostSortOrder(getActivity()))) {
            getLoaderManager().restartLoader(IfeveContract.POST_LOADER, null, this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        Bundle args = getArguments();
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (args != null) {
            this.category = args.getString(KEY_CATEGORY_STRING);
            Log.i(TAG, "onCreateView  categoryId : " + category);
        }
        View rootView = inflater.inflate(R.layout.fragment_postlist, container, false);
        postListCursorAdapter = new PostListCursorAdapter(getActivity(), null, 0);
        postListView = (ListView) rootView.findViewById(R.id.listview_post);
        postListView.setAdapter(postListCursorAdapter);
        postListView.setOnScrollListener(this);
        postListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = postListCursorAdapter.getCursor();
                if (cursor != null && cursor.moveToPosition(position)) {
                    Intent intent = new Intent(getActivity(), DetailActivity.class)
                            .putExtra("postId", cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_ID)));
                    startActivity(intent);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreated");
    }


    @Override
    public void onStart() {
        super.onStart();
        first_time = prefs.getBoolean(category, true);
        if (first_time) {
            Log.i(TAG, "onStart  categoryId : " + category + " first time : " + first_time);
            loadData();
        } else {
            Log.i(TAG, "onStart  categoryId : " + category + " first time : " + first_time);
            getLoaderManager().initLoader(IfeveContract.POST_LOADER, null, this);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated");

        Bundle args = getArguments();

        if (args != null) {
            this.category = args.getString(KEY_CATEGORY_STRING);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        Log.d(TAG, "onReceiveResult(resultCode=" + resultCode + ", resultData="
                + resultData.toString());

        switch (resultCode) {
            case SyncService.STATUS_RUNNING:


                break;
            case SyncService.STATUS_FINISHED:
                Toast.makeText(getActivity(), "成功获取所有文章列表。", Toast.LENGTH_LONG).show();
                if (isRefresh) {
                    getLoaderManager().restartLoader(IfeveContract.POST_LOADER, null, this);
                    mySwipeRefreshLayout.setRefreshing(false);
                    isRefresh = false;
                }
                if (prefs.getBoolean(category, true)) {
                    getLoaderManager().initLoader(IfeveContract.POST_LOADER, null, this);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean(category, false);
                    editor.commit();
                }

                Log.i(TAG, "onReceiveResult  categoryId : " + category + " fragment first time : " + fragment_first_time);
                break;
            case SyncService.STATUS_ERROR:
                Toast.makeText(getActivity(), "在获取文章列表的时候出现异常，请稍后再试。", Toast.LENGTH_LONG).show();
                break;

        }
    }

    @Override
    public void registerReceiver() {
        Log.i(TAG, "registerReceiver");
        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);

    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.i(TAG, "onCreateLoader");
        Uri baseUri;

        if ("index".equals(category)) {
            baseUri = IfeveContract.Posts.CONTENT_URI;
            baseUri = baseUri.buildUpon().appendQueryParameter(IfeveProvider.QUERY_PARAMETER_LIMIT, "20").build();
        } else {
            baseUri = IfeveContract.Categories.buildPostsUri(category);
        }

        post_sort_order = NormalUtil.getPreferredPostSortOrder(getActivity());
        return new CursorLoader(getActivity(), baseUri,
                null,
                null, null,
                post_sort_order);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i(TAG, "onLoadFinished");
        postListCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        postListCursorAdapter.swapCursor(null);

    }


    @Override
    public void onRefresh() {
        isRefresh = true;
        loadData();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        switch (view.getId()) {
            case android.R.id.list:

                // Make your calculation stuff here. You have all your
                // needed info from the parameters of this function.

                // Sample calculation to determine if the last
                // item is fully visible.
                final int lastItem = firstVisibleItem + visibleItemCount;
                if (lastItem == totalItemCount) {
                    if (preLast != lastItem) { //to avoid multiple calls for last item
                        Log.d("gofuckLast", "Last");
                        preLast = lastItem;
                    }
                }
        }
    }
}
