package com.ifeve.ifeveforandroid.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ifeve.ifeveforandroid.AuthorPostListActivity;
import com.ifeve.ifeveforandroid.DetailActivity;
import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.adapter.PostListCursorAdapter;
import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.provider.IfeveProvider;

/**
 * Created by penkzhou on 14-8-20.
 */
public class AuthorDetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = AuthorDetailFragment.class.getSimpleName();
    private static final int POST_LOADER = 0;
    private static final int AUTHOR_LOADER = 3;


    private TextView authorName;
    private TextView authorNickname;
    private TextView authorUrl;
    private TextView authorDesc;
    private TextView loadMoreAuthorPost;
    private String authorId;
    private LinearLayout authorPostLayout;
    private TextView authorNonePostTip;
    private ListView authorPostListView;
    private PostListCursorAdapter postListCursorAdapter;
    private LayoutInflater layoutInflater;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_author_detail, container, false);
        authorName = (TextView) rootView.findViewById(R.id.author_name);
        authorNickname = (TextView) rootView.findViewById(R.id.author_nickname);
        authorDesc = (TextView) rootView.findViewById(R.id.author_desc);
        authorUrl = (TextView) rootView.findViewById(R.id.author_url);
        authorPostLayout = (LinearLayout) rootView.findViewById(R.id.author_postList_block);
        authorNonePostTip = (TextView) rootView.findViewById(R.id.author_post_none_tip);
        loadMoreAuthorPost = (TextView) rootView.findViewById(R.id.loadMoreAuthorPost);
        postListCursorAdapter = new PostListCursorAdapter(getActivity(), null, 0);
        authorPostListView = (ListView) rootView.findViewById(R.id.authorPostList);
        authorPostListView.setAdapter(postListCursorAdapter);
        authorPostListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = postListCursorAdapter.getCursor();
                if (cursor != null && cursor.moveToPosition(position)) {
                    Intent intent = new Intent(getActivity(), DetailActivity.class)
                            .putExtra("postId", cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_ID)));
                    startActivity(intent);
                }
            }
        });
        layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Intent intent = getActivity().getIntent();
        authorId = intent.getStringExtra("authorId");
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(POST_LOADER, null, this);
        getLoaderManager().initLoader(AUTHOR_LOADER, null, this);
    }


    public void setupLoadMoreListener() {
        loadMoreAuthorPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AuthorPostListActivity.class);
                intent.putExtra(AuthorPostListActivity.AUTHOR_ID, authorId);
                intent.putExtra(AuthorPostListActivity.AUTHOR_TITLE, authorName.getText());
                startActivity(intent);
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.i(TAG, "create loader for " + i);
        Uri uri = null;
        switch (i) {
            case POST_LOADER:
                uri = IfeveContract.Authors.buildPostsDirUri(authorId);
                uri = uri.buildUpon().appendQueryParameter(IfeveProvider.QUERY_PARAMETER_LIMIT, "3").build();
                return new CursorLoader(getActivity(), uri,
                        null,
                        null, null,
                        IfeveContract.Posts.DEFAULT_SORT);
            case AUTHOR_LOADER:
                uri = IfeveContract.Authors.buildAuthorUri(authorId);
                return new CursorLoader(getActivity(), uri,
                        null,
                        null, null,
                        IfeveContract.Authors.DEFAULT_SORT);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (!cursor.moveToFirst()) {
            authorPostLayout.setVisibility(View.GONE);
            authorNonePostTip.setVisibility(View.VISIBLE);
            return;
        }
        switch (cursorLoader.getId()) {
            case POST_LOADER:
                Log.v(TAG, "In onLoadFinished for POST_LOADER");
                onPostLoadFinished(cursor);
                break;
            case AUTHOR_LOADER:
                Log.v(TAG, "In onLoadFinished for AUTHOR_LOADER");
                onAuthorLoadFinished(cursor);
                break;
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    public void onPostLoadFinished(Cursor cursor) {
        postListCursorAdapter.swapCursor(cursor);
    }


    public void onAuthorLoadFinished(Cursor cursor) {
        String authorNameValue = cursor.getString(cursor.getColumnIndex(IfeveContract.Authors.AUTHOR_NAME));
        String authorDescValue = cursor.getString(cursor.getColumnIndex(IfeveContract.Authors.AUTHOR_DESC)).trim();
        String authorUrlValue = cursor.getString(cursor.getColumnIndex(IfeveContract.Authors.AUTHOR_URL)).trim();
        String authorNicknameValue = cursor.getString(cursor.getColumnIndex(IfeveContract.Authors.AUTHOR_NICKNAME));
        authorName.setText(authorNameValue);
        authorNickname.setText(authorNicknameValue);
        if (authorDescValue.length() == 0) {
            authorDesc.setVisibility(View.GONE);
        } else {
            authorDesc.setText(authorDescValue);
        }
        if (authorUrlValue.length() == 0) {
            authorUrl.setVisibility(View.GONE);
        } else {
            authorUrl.setText(authorUrlValue);
        }
        setupLoadMoreListener();
    }

}
