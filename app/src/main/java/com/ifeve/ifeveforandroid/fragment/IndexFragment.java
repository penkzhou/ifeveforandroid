package com.ifeve.ifeveforandroid.fragment;

import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.adapter.ViewPagerAdapter;
import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.sliding.SamplePagerItem;
import com.ifeve.ifeveforandroid.sliding.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

;

/**
 * Created by penkzhou on 14-8-13.
 */
public class IndexFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private final static String TAG = IndexFragment.class.getSimpleName();
    public static int[] colors = new int[]{
            R.color.blue_dark, R.color.blue_dark, R.color.red_dark, R.color.red_light,
            R.color.green_dark, R.color.green_light, R.color.orange_dark, R.color.orange_light,
            R.color.purple_dark, R.color.purple_light};
    private View rootView;
    private List<SamplePagerItem> mTabs = new ArrayList<SamplePagerItem>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        //new FetchAllCategoryTask().execute();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG, "onActivityCreated");
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        rootView = inflater.inflate(R.layout.fragment_index, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.i(TAG, "onViewCreated");

    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        Uri baseUri = IfeveContract.Categories.CONTENT_URI;

        return new CursorLoader(getActivity(), baseUri,
                null,
                null, null,
                IfeveContract.Categories.DEFAULT_SORT);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (!cursor.moveToFirst()) return;
        int index = 0;
        do {
            String categoryTitle = cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_TITLE));
            String categoryId = cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_ID));
            mTabs.add(new SamplePagerItem(index++, categoryTitle, getResources().getColor(colors[0]), Color.GRAY, categoryId));
        } while (cursor.moveToNext());
        ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.pager);

        mViewPager.setOffscreenPageLimit(index);
        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), mTabs));

        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) rootView.findViewById(R.id.indexTabs);
        mSlidingTabLayout.setBackgroundResource(R.color.white);
        mSlidingTabLayout.setViewPager(mViewPager);

        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return mTabs.get(position).getIndicatorColor();
            }

            @Override
            public int getDividerColor(int position) {
                return mTabs.get(position).getDividerColor();
            }
        });
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }


}
