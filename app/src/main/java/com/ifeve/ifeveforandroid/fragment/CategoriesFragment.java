package com.ifeve.ifeveforandroid.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ifeve.ifeveforandroid.CategoryPostListActivity;
import com.ifeve.ifeveforandroid.MainActivity;
import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.adapter.CategoryListCursorAdapter;
import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.service.ServiceHelper;
import com.ifeve.ifeveforandroid.service.SyncService;
import com.ifeve.ifeveforandroid.util.MyResultReceiver;
import com.ifeve.ifeveforandroid.view.MySwipeRefreshLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Created by penkzhou on 14-8-24.
 */
public class CategoriesFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, MyResultReceiver.Receiver, MySwipeRefreshLayout.OnRefreshListener {
    private final static String TAG = CategoriesFragment.class.getSimpleName();
    private ListView categoryList;
    private CategoryListCursorAdapter categoryListCursorAdapter;
    private View mHeader, mFooter;
    private MyResultReceiver myResultReceiver;
    private MySwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(IfeveContract.CATEGORY_LOADER, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_categories, container, false);
        swipeRefreshLayout = (MySwipeRefreshLayout) rootView.findViewById(R.id.categoryListRefresh);
        swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setEnabled(true);
        categoryList = (ListView) rootView.findViewById(R.id.categoriesList);
        categoryListCursorAdapter = new CategoryListCursorAdapter(getActivity(), null, 0);
        categoryList.setAdapter(categoryListCursorAdapter);
        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = categoryListCursorAdapter.getCursor();
                if (cursor != null && cursor.moveToPosition(position)) {
                    Intent intent = new Intent(getActivity(), CategoryPostListActivity.class);
                    String categoryId = cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_ID));
                    String categoryTitle = cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_TITLE));
                    intent.putExtra(CategoryPostListActivity.CATEGORY_ID, categoryId);
                    intent.putExtra(CategoryPostListActivity.CATEGORY_TITLE, categoryTitle);
                    startActivity(intent);
                }
            }
        });
        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.i(TAG, "onCreateLoader");
        Uri baseUri = IfeveContract.Categories.CONTENT_URI;

        return new CursorLoader(getActivity(), baseUri,
                null,
                null, null,
                IfeveContract.Categories.DEFAULT_SORT);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        Log.i(TAG, "onLoadFinished");
        categoryListCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        categoryListCursorAdapter.swapCursor(null);
    }


    public void updateHeader(int message, boolean syncing) {

        TextView progressStatus = (TextView) getActivity().findViewById(
                R.id.progress_status);

        progressStatus.setText(message);

        TextView timeUpdated = (TextView) getActivity().findViewById(
                R.id.progress_update_time);
        timeUpdated.setText(getSyncTime());

        ProgressBar bar = (ProgressBar) getActivity().findViewById(
                R.id.progressBar1);
//        if (!syncing) {
//            bar.setVisibility(ProgressBar.INVISIBLE);
//            getListView().removeFooterView(mFooter);
//        } else {
//            bar.setVisibility(ProgressBar.VISIBLE);
//            getListView().addFooterView(mFooter);
//
//        }

    }

    public String getSyncTime() {
        SharedPreferences prefs = getActivity().getSharedPreferences(
                "synctime", Context.MODE_PRIVATE);
        return prefs.getString("last_time", "never");
    }

    public void updateSyncTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        SharedPreferences.Editor e = getActivity().getSharedPreferences("synctime",
                Context.MODE_PRIVATE).edit();
        e.putString("last_time", formattedDate);
        e.commit();

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case SyncService.STATUS_ERROR:
                Toast.makeText(getActivity(), "在获取类别数据的时候出现异常，请稍后再试。", Toast.LENGTH_LONG).show();
                break;
            case SyncService.STATUS_RUNNING:
                break;
            case SyncService.STATUS_FINISHED:
                Toast.makeText(getActivity(), "成功刷新所有类别数据", Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);
                getLoaderManager().restartLoader(IfeveContract.CATEGORY_LOADER, null, this);
                break;
        }
    }

    @Override
    public void registerReceiver() {
        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);
    }

    @Override
    public void onRefresh() {
        registerReceiver();
        final String categoryUrl = "http://ifeve.com/api/get_category_index/";
        ServiceHelper.execute(getActivity(), myResultReceiver, MainActivity.CATEGORY_TOKEN, categoryUrl);
    }
}
