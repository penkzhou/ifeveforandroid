package com.ifeve.ifeveforandroid.sliding;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ifeve.ifeveforandroid.fragment.PostListFragment;

public class SamplePagerItem {
    public static final String FRAGMENT_FIRST_TIME = "fragment_first_time";

    private final CharSequence mTitle;
    private final int mIndicatorColor;
    private final int mDividerColor;
    private final int position;
    private final String categoryId;

    public SamplePagerItem(int position, CharSequence title, int indicatorColor, int dividerColor, String categoryId) {
        this.mTitle = title;
        this.position = position;
        this.mIndicatorColor = indicatorColor;
        this.mDividerColor = dividerColor;
        this.categoryId = categoryId;
    }

    public Fragment createFragment() {
        Fragment fragment = PostListFragment.newInstance(categoryId);
        Bundle bundle = fragment.getArguments();
        if (bundle != null) {
            //record every fragment's first_time arg.
            bundle.putInt(FRAGMENT_FIRST_TIME, 0);
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    public CharSequence getTitle() {
        return mTitle;
    }

    public int getIndicatorColor() {
        return mIndicatorColor;
    }

    public int getDividerColor() {
        return mDividerColor;
    }
}
