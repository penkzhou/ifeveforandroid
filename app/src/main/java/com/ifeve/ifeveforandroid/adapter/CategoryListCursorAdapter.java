package com.ifeve.ifeveforandroid.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.provider.IfeveContract;

/**
 * Created by penkzhou on 14-8-23.
 */
public class CategoryListCursorAdapter extends CursorAdapter {
    public CategoryListCursorAdapter(Context context, Cursor c, int flag) {
        super(context, c, flag);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.categoryTitle.setText(cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_TITLE)));
        String categoryDescStr = cursor.getString(cursor.getColumnIndex(IfeveContract.Categories.CATEGORY_DESC));
        if (categoryDescStr.trim().length() == 0) {
            viewHolder.categoryDesc.setVisibility(View.GONE);
        } else {
            viewHolder.categoryDesc.setText(categoryDescStr);
        }
    }

    public static class ViewHolder {
        public TextView categoryTitle, categoryDesc, categoryCount;

        public ViewHolder(View view) {
            categoryDesc = (TextView) view.findViewById(R.id.category_desc);
            categoryTitle = (TextView) view.findViewById(R.id.category_title);
        }
    }
}
