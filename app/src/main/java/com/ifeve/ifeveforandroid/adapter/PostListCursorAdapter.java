package com.ifeve.ifeveforandroid.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ifeve.ifeveforandroid.R;
import com.ifeve.ifeveforandroid.provider.IfeveContract;

/**
 * Created by penkzhou on 14-8-23.
 */
public class PostListCursorAdapter extends CursorAdapter {
    public PostListCursorAdapter(Context context, Cursor c, int flag) {
        super(context, c, flag);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view = LayoutInflater.from(context).inflate(R.layout.post_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        return view;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        String postDateStr = cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_CREATE_DATE));
        postDateStr = postDateStr.split(" ")[0];
        viewHolder.postTitle.setText(cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_TITLE)));
        viewHolder.postDate.setText(postDateStr);
        viewHolder.postCommentCount.setText(cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_COMMENT_COUNT)));
        viewHolder.postViewCount.setText(cursor.getString(cursor.getColumnIndex(IfeveContract.Posts.POST_VIEW_COUNT)));
    }

    public static class ViewHolder {
        public TextView postTitle, postDate, postCommentCount, postViewCount;

        public ViewHolder(View view) {
            postCommentCount = (TextView) view.findViewById(R.id.post_comment_count);
            postViewCount = (TextView) view.findViewById(R.id.post_view_count);
            postDate = (TextView) view.findViewById(R.id.post_date);
            postTitle = (TextView) view.findViewById(R.id.post_title);
        }
    }
}
