package com.ifeve.ifeveforandroid.process;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.os.RemoteException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by penkzhou on 14-8-22.
 */
public abstract class Processor {

    private final String mAuthority;

    public Processor(String authority) {
        mAuthority = authority;
    }

    /**
     * Parse the given {@link JSONObject}, turning into a series of
     * {@link ContentProviderOperation} that are immediately applied using the
     * given {@link ContentResolver}.
     */
    public void parseAndApply(JSONObject parser, ContentResolver resolver)
            throws ProcessorException {
        try {
            final ArrayList<ContentProviderOperation> batch = parse(parser, resolver);
            resolver.applyBatch(mAuthority, batch);

        } catch (ProcessorException e) {
            throw e;
        } catch (JSONException e) {
            throw new ProcessorException("Problem parsing JSON response", e);
        } catch (IOException e) {
            throw new ProcessorException("Problem reading response", e);
        } catch (RemoteException e) {
            // Failed binder transactions aren't recoverable
            throw new RuntimeException("Problem applying batch operation", e);
        } catch (OperationApplicationException e) {
            // Failures like constraint violation aren't recoverable
            // TODO: write unit tests to exercise full provider
            // TODO: consider catching version checking asserts here, and then
            // wrapping around to retry parsing again.
            throw new RuntimeException("Problem applying batch operation", e);
        }
    }

    /**
     * Parse the given {@link org.json.JSONObject}, returning a set of
     * {@link ContentProviderOperation} that will bring the
     * {@link ContentProvider} into sync with the parsed data.
     */
    public abstract ArrayList<ContentProviderOperation> parse(JSONObject parser,
                                                              ContentResolver resolver) throws JSONException, IOException;

    /**
     * General {@link IOException} that indicates a problem occured while
     * parsing or applying an {@link JSONObject}.
     */
    public static class ProcessorException extends IOException {
        public ProcessorException(String message) {
            super(message);
        }

        public ProcessorException(String message, Throwable cause) {
            super(message);
            initCause(cause);
        }

        @Override
        public String toString() {
            if (getCause() != null) {
                return getLocalizedMessage() + ": " + getCause();
            } else {
                return getLocalizedMessage();
            }
        }
    }
}
