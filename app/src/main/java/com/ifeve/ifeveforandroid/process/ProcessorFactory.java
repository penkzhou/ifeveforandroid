package com.ifeve.ifeveforandroid.process;

import com.ifeve.ifeveforandroid.MainActivity;

/**
 * Created by penkzhou on 14-8-23.
 */
public class ProcessorFactory {
    public static Processor createProcessor(int action) throws UnsupportedOperationException {


		/*SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String customer_id = preferences.getString("customer_id", "");

		if (customer_id.equals("")) {
			throw new RuntimeException("Something went wrong when reading the customer id.");
		}*/

        switch (action) {
            case MainActivity.TAG_TOKEN:
                return new TagsProcessor();
            case MainActivity.CATEGORY_TOKEN:
                return new CategoriesProcessor();
            case MainActivity.AUTHOR_TOKEN:
                return new AuthorsProcessor();
            case MainActivity.POST_TOKEN:
                return new PostsProcessor();
            case MainActivity.POST_CLEAN_TOKEN:
                return new PostsProcessor(true);
            case MainActivity.REFRESH_POST_TOKEN:
                return new RefreshPostsProcessor();
            case MainActivity.REFRESH_POST_CLEAN_TOKEN:
                return new RefreshPostsProcessor(true);
            default:
                throw new UnsupportedOperationException("No processor was found to match the requested URI. " + action);

        }


    }

}
