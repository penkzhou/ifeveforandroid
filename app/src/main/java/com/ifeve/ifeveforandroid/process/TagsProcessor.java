package com.ifeve.ifeveforandroid.process;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.util.Log;

import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Tags;
import com.ifeve.ifeveforandroid.util.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by penkzhou on 14-8-23.
 */
public class TagsProcessor extends Processor {
    private static final String TAG = "TagsProcessor";

    public TagsProcessor() {
        super(IfeveContract.CONTENT_AUTHORITY);
    }


    @Override
    public ArrayList<ContentProviderOperation> parse(JSONObject parser, ContentResolver resolver) throws JSONException, IOException {
        final ArrayList<ContentProviderOperation> batch = Lists.newArrayList();
        int numEvents = 0;
        if ("ok".equals(parser.getString("status"))) {
            numEvents = parser.getInt("count");
        }

        if (numEvents > 0) {
            JSONArray tags = parser.getJSONArray("tags");
            Log.i(TAG, "Updating tags data");


            /**
             *
             * "id":377
             * "slug":"acknowledgments"
             * "title":"Acknowledgments"
             * "description":""
             * "post_count":1
             */


            for (int j = 0; j < tags.length(); j++) {
                // Insert tag info
                JSONObject tag = tags.getJSONObject(j);
                String tagId = tag.getString("id");
                final ContentProviderOperation.Builder builder = ContentProviderOperation
                        .newInsert(Tags.CONTENT_URI)
                        .withValue(IfeveContract.SyncColumns.UPDATED, System.currentTimeMillis())
                        .withValue(Tags.TAG_ID, tagId)
                        .withValue(Tags.TAG_DESC, tag.getString("description"))
                        .withValue(Tags.TAG_POST_COUNT, tag.getInt("post_count"))
                        .withValue(Tags.TAG_SLUG, tag.getString("slug"))
                        .withValue(Tags.TAG_TITLE, tag.getString("title"));
                batch.add(builder.build());
            }
        }

        return batch;
    }
}
