package com.ifeve.ifeveforandroid.process;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.util.Log;

import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Categories;
import com.ifeve.ifeveforandroid.util.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by penkzhou on 14-8-23.
 */
public class CategoriesProcessor extends Processor {
    private static final String TAG = "CategoriesProcessor";

    public CategoriesProcessor() {
        super(IfeveContract.CONTENT_AUTHORITY);
    }


    @Override
    public ArrayList<ContentProviderOperation> parse(JSONObject parser, ContentResolver resolver) throws JSONException, IOException {
        final ArrayList<ContentProviderOperation> batch = Lists.newArrayList();
        int numEvents = 0;
        if ("ok".equals(parser.getString("status"))) {
            numEvents = parser.getInt("count");
        }

        if (numEvents > 0) {
            JSONArray categories = parser.getJSONArray("categories");
            Log.i(TAG, "Updating categories data");


            /**
             * description: ""
             * id: 366
             * parent: 0
             * post_count: 1
             * slug: "cpu-2"
             * title: "CPU"
             */


            for (int j = 0; j < categories.length(); j++) {
                // Insert category info
                JSONObject category = categories.getJSONObject(j);
                String categoryId = category.getString("id");
                final ContentProviderOperation.Builder builder = ContentProviderOperation
                        .newInsert(Categories.CONTENT_URI)
                        .withValue(IfeveContract.SyncColumns.UPDATED, System.currentTimeMillis())
                        .withValue(Categories.CATEGORY_ID, categoryId)
                        .withValue(Categories.CATEGORY_DESC, category.getString("description"))
                        .withValue(Categories.CATEGORY_PARENT, category.getString("parent"))
                        .withValue(Categories.CATEGORY_POST_COUNT, category.getInt("post_count"))
                        .withValue(Categories.CATEGORY_TITLE, category.getString("title"))
                        .withValue(Categories.CATEGORY_SLUG, category.getString("slug"));
                batch.add(builder.build());
            }
        }

        return batch;
    }
}
