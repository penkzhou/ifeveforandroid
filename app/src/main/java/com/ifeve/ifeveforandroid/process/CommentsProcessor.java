package com.ifeve.ifeveforandroid.process;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.util.Log;

import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Comments;
import com.ifeve.ifeveforandroid.util.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by penkzhou on 14-8-23.
 */
public class CommentsProcessor extends Processor {
    private static final String TAG = "CommentsProcessor";

    public CommentsProcessor() {
        super(IfeveContract.CONTENT_AUTHORITY);
    }


    @Override
    public ArrayList<ContentProviderOperation> parse(JSONObject parser, ContentResolver resolver) throws JSONException, IOException {
        final ArrayList<ContentProviderOperation> batch = Lists.newArrayList();
        int numEvents = 0;
        if ("ok".equals(parser.getString("status"))) {
            numEvents = parser.getInt("count");
        }

        if (numEvents > 0) {
            JSONArray comments = parser.getJSONArray("comments");
            Log.i(TAG, "Updating comments data");


            /**
             *
             * content: "<p>这种实战非常有价值，可以写一篇详细的文章发表在并发网上。</p>↵"
             * date: "2014-04-28 00:04:00"
             * id: 18531
             * name: "方 腾飞"
             * parent: 18523
             * url: "http://weibo.com/kirals"
             */


            for (int j = 0; j < comments.length(); j++) {
                // Insert comment info
                JSONObject comment = comments.getJSONObject(j);
                String commentId = comment.getString("id");
                final ContentProviderOperation.Builder builder = ContentProviderOperation
                        .newInsert(Comments.CONTENT_URI)
                        .withValue(IfeveContract.SyncColumns.UPDATED, System.currentTimeMillis())
                        .withValue(Comments.COMMENT_ID, commentId)
                        .withValue(Comments.COMMENT_DATE, comment.getString("date"))
                        .withValue(Comments.COMMENT_NAME, comment.getString("name"))
                        .withValue(Comments.COMMENT_PARENT, comment.getString("parent"))
                        .withValue(Comments.COMMENT_CONTENT, comment.getString("content"))
                        .withValue(Comments.COMMENT_URL, comment.getString("url"));
                batch.add(builder.build());
            }
        }

        return batch;
    }
}
