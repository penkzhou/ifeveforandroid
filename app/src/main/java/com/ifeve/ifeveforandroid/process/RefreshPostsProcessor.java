package com.ifeve.ifeveforandroid.process;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;

import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Posts;
import com.ifeve.ifeveforandroid.provider.IfeveDatabase;
import com.ifeve.ifeveforandroid.util.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by penkzhou on 14-8-23.
 */
public class RefreshPostsProcessor extends Processor {
    private static final String TAG = "PostsProcessor";
    private boolean mClean_db = false;

    public RefreshPostsProcessor() {
        super(IfeveContract.CONTENT_AUTHORITY);
    }

    public RefreshPostsProcessor(boolean clean) {
        super(IfeveContract.CONTENT_AUTHORITY);
        mClean_db = clean;
    }

    @Override
    public ArrayList<ContentProviderOperation> parse(JSONObject parser, ContentResolver resolver) throws JSONException, IOException {
        final ArrayList<ContentProviderOperation> batch = Lists.newArrayList();
        int numEvents = 0;
        if ("ok".equals(parser.getString("status"))) {
            numEvents = parser.getInt("count");
        }

        if (numEvents > 0) {
            JSONArray posts = parser.getJSONArray("posts");
            Log.i(TAG, "Updating posts data");
//            JSONObject cat = parser.getJSONObject("category");
//            String categoryId = cat.getString("id");
            if (mClean_db) {
//                final Uri categoryPostUri = IfeveContract.Categories.buildPostsUri(categoryId);
//                batch.add(ContentProviderOperation
//                        .newDelete(categoryPostUri)
//                        .build());
            }


            // Maintain a list of created post IDs
            Set<String> postIds = new HashSet<String>();

            for (int j = 0; j < posts.length(); j++) {
                // Insert post info
                JSONObject post = posts.getJSONObject(j);
                String postId = post.getString("id");
                final ContentProviderOperation.Builder builder = ContentProviderOperation
                        .newInsert(Posts.CONTENT_URI)
                        .withValue(IfeveContract.SyncColumns.UPDATED, System.currentTimeMillis())
                        .withValue(Posts.POST_ID, postId)
                        .withValue(Posts.POST_TYPE, post.getString("type"))
                        .withValue(Posts.POST_TITLE, post.getString("title"))
                        .withValue(Posts.POST_COMMENT_COUNT, post.getInt("comment_count"))
                        .withValue(Posts.POST_COMMENT_STATUS, post.getString("comment_status"))
                        .withValue(Posts.POST_CONTENT, post.getString("content"))
                        .withValue(Posts.POST_CREATE_DATE, post.getString("date"))
                        .withValue(Posts.POST_EXCERPT, post.getString("excerpt"))
                        .withValue(Posts.POST_MODIFY_DATE, post.getString("modified"))
                        .withValue(Posts.POST_SLUG, post.getString("slug"))
                        .withValue(Posts.POST_STATUS, post.getString("status"))
                        .withValue(Posts.POST_VIEW_COUNT, post.getJSONObject("custom_fields").getJSONArray("views").getInt(0))
                        .withValue(Posts.POST_URL, post.getString("url"))
                        .withValue(Posts.AUTHOR_ID, post.getJSONObject("author").getString("id"));


                batch.add(builder.build());


                // Replace all post categories
                final Uri postCategoriesUri = Posts.buildCategoriesDirUri(postId);
                batch.add(ContentProviderOperation
                        .newDelete(postCategoriesUri)
                        .build());
                JSONArray categories = post.getJSONArray("categories");
                if (categories.length() > 0) {
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject category = categories.getJSONObject(i);
                        batch.add(ContentProviderOperation.newInsert(postCategoriesUri)
                                .withValue(IfeveDatabase.PostsCategories.POST_ID, postId)
                                .withValue(IfeveDatabase.PostsCategories.CATEGORY_ID, category.getString("id")).build());
                    }
                }

                // Replace all post tags
                final Uri postTagsUri = Posts.buildTagsAtDirUri(postId);
                batch.add(ContentProviderOperation.newDelete(postTagsUri).build());
                JSONArray tags = post.getJSONArray("tags");
                if (tags.length() > 0) {
                    for (int i = 0; i < tags.length(); i++) {
                        JSONObject tag = tags.getJSONObject(i);
                        String tagId = tag.getString("id");
                        batch.add(ContentProviderOperation.newInsert(postTagsUri)
                                .withValue(IfeveDatabase.PostsTags.POST_ID, postId)
                                .withValue(IfeveDatabase.PostsTags.TAG_ID, tagId).build());
                    }
                }


                // Replace all post comments
                final Uri postCommentsUri = Posts.buildCommentsDirUri(postId);
                batch.add(ContentProviderOperation.newDelete(postCommentsUri).build());
                JSONArray comments = post.getJSONArray("comments");
                if (comments.length() > 0) {
                    for (int i = 0; i < comments.length(); i++) {
                        JSONObject comment = comments.getJSONObject(i);
                        String commentId = comment.getString("id");
                        batch.add(ContentProviderOperation.newInsert(IfeveContract.Comments.CONTENT_URI)
                                .withValue(IfeveContract.SyncColumns.UPDATED, System.currentTimeMillis())
                                .withValue(IfeveContract.Comments.COMMENT_ID, commentId)
                                .withValue(IfeveContract.Comments.COMMENT_DATE, comment.getString("date"))
                                .withValue(IfeveContract.Comments.COMMENT_NAME, comment.getString("name"))
                                .withValue(IfeveContract.Comments.COMMENT_PARENT, comment.getString("parent"))
                                .withValue(IfeveContract.Comments.COMMENT_CONTENT, comment.getString("content"))
                                .withValue(IfeveContract.Comments.COMMENT_URL, comment.getString("url")).build());
                        batch.add(ContentProviderOperation.newInsert(postCommentsUri)
                                .withValue(IfeveDatabase.PostsComments.POST_ID, postId)
                                .withValue(IfeveDatabase.PostsComments.COMMENT_ID, commentId).build());
                    }
                }
            }
        }

        return batch;
    }
}
