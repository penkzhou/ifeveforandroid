package com.ifeve.ifeveforandroid.process;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.util.Log;

import com.ifeve.ifeveforandroid.provider.IfeveContract;
import com.ifeve.ifeveforandroid.provider.IfeveContract.Authors;
import com.ifeve.ifeveforandroid.util.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by penkzhou on 14-8-23.
 */
public class AuthorsProcessor extends Processor {
    private static final String TAG = "AuthorsProcessor";

    public AuthorsProcessor() {
        super(IfeveContract.CONTENT_AUTHORITY);
    }


    @Override
    public ArrayList<ContentProviderOperation> parse(JSONObject parser, ContentResolver resolver) throws JSONException, IOException {
        final ArrayList<ContentProviderOperation> batch = Lists.newArrayList();
        int numEvents = 0;
        if ("ok".equals(parser.getString("status"))) {
            numEvents = parser.getInt("count");
        }

        if (numEvents > 0) {
            JSONArray authors = parser.getJSONArray("authors");
            Log.i(TAG, "Updating authors data");


            /**
             *
             * description: ""
             * first_name: ""
             * id: 2154
             * last_name: ""
             * name: "owenludong"
             * nickname: "owenludong"
             * slug: "owenludong"
             * url: "http://weibo.com/1884112540"
             */


            for (int j = 0; j < authors.length(); j++) {
                // Insert author info
                JSONObject author = authors.getJSONObject(j);
                String authorId = author.getString("id");
                final ContentProviderOperation.Builder builder = ContentProviderOperation
                        .newInsert(Authors.CONTENT_URI)
                        .withValue(IfeveContract.SyncColumns.UPDATED, System.currentTimeMillis())
                        .withValue(Authors.AUTHOR_ID, authorId)
                        .withValue(Authors.AUTHOR_DESC, author.getString("description"))
                        .withValue(Authors.AUTHOR_FIRST_NAME, author.getString("first_name"))
                        .withValue(Authors.AUTHOR_LAST_NAME, author.getString("last_name"))
                        .withValue(Authors.AUTHOR_NAME, author.getString("name"))
                        .withValue(Authors.AUTHOR_NICKNAME, author.getString("nickname"))
                        .withValue(Authors.AUTHOR_SLUG, author.getString("slug"))
                        .withValue(Authors.AUTHOR_URL, author.getString("url"));
                batch.add(builder.build());
            }
        }

        return batch;
    }
}
