package com.ifeve.ifeveforandroid.service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.ifeve.ifeveforandroid.process.Executor;
import com.ifeve.ifeveforandroid.process.IfeveExecutor;
import com.ifeve.ifeveforandroid.process.ProcessorFactory;

/**
 * Created by penkzhou on 14-8-23.
 */
public class SyncService extends IntentService {

    public static final String EXTRA_STATUS_RECEIVER = "com.ifeve.ifeveforandroid.STATUS_RECEIVER";
    public static final int STATUS_RUNNING = 0x1;
    public static final int STATUS_ERROR = 0x2;
    public static final int STATUS_FINISHED = 0x3;
    private static final String TAG = "SyncService";
    private boolean abort = false;
    private Executor mExecutor;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public SyncService() {
        super(TAG);
    }


    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate called");
        super.onCreate();
        abort = false;
        ContentResolver resolver = getContentResolver();
        mExecutor = new IfeveExecutor(getApplicationContext(), resolver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        abort = true;
        Log.d(TAG, "SyncService onDestroy Called");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent(intent=" + intent.toString() + ")");

        ResultReceiver receiver = intent.getParcelableExtra(EXTRA_STATUS_RECEIVER);
        if (receiver != null) {
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
        }

        final long startREST = System.currentTimeMillis();

        try {
            mExecutor.execute(intent.getExtras().getString("api_url"), ProcessorFactory.createProcessor(intent.getExtras().getInt("token")));
            final long stopREST = System.currentTimeMillis();

            Log.d(TAG, "Remote syncing took " + (stopREST - startREST) + "ms");

        } catch (Exception e) {
            Log.e(TAG, "Problem while syncing", e);

            if (receiver != null && !abort) {
                Bundle bundle = new Bundle();
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, bundle);
                return;
            }
        }

        Log.d(TAG, "Syncing finished.");

        if (receiver != null && !abort) {
            receiver.send(STATUS_FINISHED, Bundle.EMPTY);
        }


    }
}
