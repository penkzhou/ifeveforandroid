package com.ifeve.ifeveforandroid;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

/**
 * Created by penkzhou on 14-8-24.
 */
public class AboutActivity extends ActionBarActivity {
    private static final String TAG = AboutActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

}
